<title>Admin Home Page</title>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand/logo -->
		<a class="navbar-brand"> <img
			src="https://upload.wikimedia.org/wikipedia/commons/6/63/Harokopio_University_Logo.png"
			alt="logo" style="width: 40px;">
		</a> <span class="navbar-text"><i class="fa fa-user-circle"></i>
			Welcome Admin </span>
		<!-- Links -->
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="NewRole"><i
					class="fa fa-plus"></i>Create Role</a></li>
			<li class="nav-item"><a class="nav-link" href="NewAction"><i
					class="fa fa-plus"></i>Create Action</a></li>
			<li class="nav-item"><a class="nav-link" href="SeeRights"><i
					class="fa fa-paperclip"></i>See Rights </a></li>
		</ul>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item"><a class="nav-link"
				href="<c:url value="/logout" />"><i class="fa fa-sign-out"></i>Logout</a></li>
		</ul>
	</nav>
	<c:if test="${not empty message}">
		<div class="alert alert-warning">
			<strong>Warning!</strong> ${message}
		</div>
	</c:if>
	<div class="container">
		<h2>Database Tables</h2>
		<br>
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item"><a class="nav-link active"
				data-toggle="tab" href="#home">Roles</a></li>
			<li class="nav-item"><a class="nav-link" data-toggle="tab"
				href="#menu1">Actions</a></li>
			<li class="nav-item"><a class="nav-link" data-toggle="tab"
				href="#menu2">Users</a></li>
		</ul>
		<div class="tab-content">
			<div id="home" class="container tab-pane active">
				<br>
				<div class="container">
					<h2>
						<i class="fa fa-graduation-cap"></i>Roles List
					</h2>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>RoleId</th>
								<th>Role_title</th>
								<th>Role_rights</th>
								<th>Actions</th>
								<th><a href="NewRole" class="button">Create a role</a></th>
							</tr>
						</thead>
						<tbody>
							<!-- loop over and print our customers -->
							<c:forEach var="Role" items="${roles}">

								<tr>
									<td>${Role.role_id}</td>
									<td>${Role.role_title}</td>
									<td>${Role.role_rights}</td>
									<td><button type="button" class="btn btn-outline-primary">
											<a
												href="<c:url value="/admin/NewUser/${Role.role_id}"></c:url>"><i
												class="fa fa-user-plus"></i> Add User</a>
										</button>
										<button type="button" class="btn btn-outline-primary">
											<a
												href="<c:url value="/admin/deleterole/${Role.role_id}"></c:url>">
												<i class="fa fa-trash-o"></i>Delete role
											</a>
										</button>
										<button type="button" class="btn btn-outline-primary">
											<a
												href="<c:url value="/admin/role/get/${Role.role_id}"></c:url>">
												<i class="fa fa-cog"></i>Modify role rights
											</a>
										</button>
								</tr>

							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div id="menu1" class="container tab-pane fade">
				<br>
				<div class="container">
					<h2>
						<i class="fa fa-cogs"></i>Actions List
					</h2>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>ActionId</th>
								<th>ActionTitle</th>
								<th>ActionRights</th>
								<th>Actions</th>
								<th><a href="NewAction" class="button">Create a new
										Action</a></th>
							</tr>
						</thead>
						<!-- loop over and print our customers -->
						<tbody>
							<c:forEach var="Action" items="${actions}">

								<tr>
									<td>${Action.action_id}</td>
									<td>${Action.action_title}</td>
									<td>${Action.action_rights}</td>
									<td><button type="button" class="btn btn-outline-primary">
											<a
												href="<c:url value="/admin/deleteAction/${Action.action_id}"></c:url>"><iclass="unhideicon">
												</i> <i class="fa fa-trash-o"></i>Delete</a>
										</button>
								</tr>

							</c:forEach>
						</tbody>
					</table>
				</div>

			</div>
			<div id="menu2" class="container tab-pane fade">
				<br>
				<div class="container">
					<h2>
						<i class="fa fa-users"></i>Users List
					</h2>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Role</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
								<th>Username</th>
								<th>Department</th>
								<th>ActionsList</th>
								<th>Actions</th>
							</tr>
						</thead>
						<!-- loop over and print our customers -->
						<tbody>
							<c:forEach var="User" items="${users}">

								<tr>
									<td>${User.role}</td>
									<td>${User.first_name}</td>
									<td>${User.last_name}</td>
									<td>${User.email}</td>
									<td>${User.username}</td>
									<td>${User.department}</td>
									<td>${User.actions}</td>
									<td><c:if test="${User.role != 'ROLE_ADMIN'}">
											<button type="button" class="btn btn-outline-primary">
												<a href="<c:url value="/admin/${User.id}"></c:url>"><i
													class="fa fa-user-times"></i>Delete</a>
											</button><button type="button" class="btn btn-outline-primary">
												<a href="<c:url value="/admin/get/${User.id}"></c:url>"><i
													class="fa fa-cog"></i> Modify</a>
											</button>
										
										</c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>