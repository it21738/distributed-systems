<title>Home-Menu</title>
</head>
<style>
@import
	url('https://fonts.googleapis.com/css?family=Kalam&display=swap');
</style>
<style>
.module {
	display: grid;
	position: relative;
}

.module::before {
	content: "";
	width: 100%;
	height: 100%;
	position: fixed;
	background-image:
		url("https://www.neolaia.gr/wp-content/uploads/2013/01/xarokopeio.jpg");
	background-size: cover;
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
	filter: grayscale(100%);
}

.module-inside {
	position: relative;
}

.imgcontainer {
	text-align: center;
	margin: 24px 0 12px 0;
}

#h1 {
	font-family: 'Kalam', cursive;
}

form i.fa {
	position: absolute;
	top: 6px;
	left: 20px;
	font-size: 22px;
	z-index: 9999;
}

.form-control {
	position: relative;
	padding-left: 45px !important;
}
</style>
<body>
	<sec:authorize access="isAuthenticated()">
		<div class="ui segment">
			User:
			<sec:authentication property="principal.username" />
			, Role:
			<sec:authentication property="principal.authorities" />
		</div>
	</sec:authorize>
	<div class="module">
		<div class="module-inside">
			<div class="shadow-lg p-4">
				<div class="navbar navbar-inverse navbar-fixed-top" id=nav1>
					<a class="navbar-brand"> <img
						src="https://pagecdn.io/repo/10521952be691de3ac6b/wp-content/uploads/2018/03/logo_hua_full.png"
						style="width: 200px;"></a> <span id="h1" class="navbar-text"><font
						size="6"> Welcome Supervisors</font> </span>

				</div>
			</div>
			<br> <br>
			<div class="container-fluid">
				<div class="row justify-content-center custom-margin">
					<div class="col-sm-6 col-md-4">
						<form  name="login" action="${pageContext.request.contextPath}/login"
							method="POST" class="shadow-lg p-4">
							<h1 id="h1">Log-in Here</h1>
							<div class="form-group">
								<div class="imgcontainer">
									<img
										src="https://upload.wikimedia.org/wikipedia/commons/6/63/Harokopio_University_Logo.png">
								</div>
								<label for="username" class="pl-2 font-weight-bold" id="h1">Username</label>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<i class="fa fa-user-o"></i> <input id="username"
												class="form-control custom-input" name="username" value=""
												placeholder="Username " required autofocus
												style="border-radius: 30px;">
										</div>
									</div>
								</div>
								<label for="password" class="pl-2 font-weight-bold" id="h1">Password</label>
								<div class="row">
									<div class="col-md-12">
										<i class="fa fa-lock"></i> <input id="password"
											type="password" class="form-control custom-input"
											name="password" placeholder="Password" required
											style="border-radius: 30px;">

									</div>
								</div>
							</div>
							<c:if test="${not empty errorMessge}"><div style="color:red; font-weight: bold; margin: 30px 0px;">${errorMessge}</div></c:if>
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
							<font color="red">${message}</font>
							<button type="submit"
								class="btn btn-outline-success mt-3 btn-block shadow-sm font-weight-bold">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>