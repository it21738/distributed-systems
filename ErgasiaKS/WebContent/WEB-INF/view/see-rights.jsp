<title>Rights</title>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand/logo -->
		<a class="navbar-brand"> <img
			src="https://upload.wikimedia.org/wikipedia/commons/6/63/Harokopio_University_Logo.png"
			alt="logo" style="width: 40px;">
		</a>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="entry"><i class="fa fa-home"></i>Home</a></li>
		</ul>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item"><a class="nav-link" href="<c:url value="/logout" />"><i class="fa fa-sign-out"></i>Logout</a>
			</li>
		</ul>
	</nav>
	<div class="container">
		<br>
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item"><a class="nav-link active"
				data-toggle="tab" href="#home">ManagerRights</a></li>
			<li class="nav-item"><a class="nav-link" data-toggle="tab"
				href="#menu1">ChiefRights</a></li>
			<li class="nav-item"><a class="nav-link" data-toggle="tab"
				href="#menu2">StudentRights</a></li>
		</ul>
		<div class="tab-content">
			<div id="home" class="container tab-pane active">
				<br>
				<div class="container">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>UserId</th>
								<th>Activate_student</th>
								<th>Handle_request</th>
							</tr>
						</thead>
						<tbody>
							<!-- loop over and print our customers -->
							<c:forEach var="Managerright" items="${managerrights}">

								<tr>
									<td>${Managerright.user_id}</td>
									<td>${Managerright.activate_student}</td>
									<td>${Managerright.handle_request}</td>

								</tr>

							</c:forEach>
						</tbody>
					</table>
				</div>

			</div>
			<div id="menu1" class="container tab-pane fade">
				<br>
				<div class="container">
					<table class="table table-striped">
						<thead>
							<th>User_id</th>
							<th>Activate_student</th>
							<th>Handle_request</th>
							<th>Update_positions</th>
						</thead>
						<!-- loop over and print our customers -->
						<tbody>
							<c:forEach var="Chiefrights" items="${chiefrights}">

								<tr>
									<td>${Chiefrights.user_id}</td>
									<td>${Chiefrights.activate_student}</td>
									<td>${Chiefrights.handle_request}</td>
									<td>${Chiefrights.update_pos}</td>
								</tr>

							</c:forEach>
						</tbody>
					</table>
				</div>

			</div>
			<div id="menu2" class="container tab-pane fade">
				<br>
				<div class="container">
					<table class="table table-striped">
						<thead>
							<th>User_id</th>
							<th>Create_request</th>
							<th>View_ranking</th>
							<th>View_results</th>
							<th>Change_com</th>
						</thead>
						<!-- loop over and print our customers -->
						<tbody>
							<c:forEach var="Studentrights" items="${studentrights}">

								<tr>
									<td>${Studentrights.user_id}</td>
									<td>${Studentrights.create_req}</td>
									<td>${Studentrights.view_rank}</td>
									<td>${Studentrights.view_res}</td>
									<td>${Studentrights.change_com}</td>

								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>