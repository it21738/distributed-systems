<title>New User</title>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand/logo -->
		<a class="navbar-brand"> <img
			src="https://upload.wikimedia.org/wikipedia/commons/6/63/Harokopio_University_Logo.png"
			alt="logo" style="width: 40px;">
		</a>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link"
				href="/ErgasiaKS/admin/entry"><i class="fa fa-home"></i>Home</a></li>
		</ul>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item"><a class="nav-link"
				href="<c:url value="/logout" />"><i class="fa fa-sign-out"></i>Logout</a></li>
		</ul>
	</nav>
	<c:if test="${not empty message}">
		<div class="alert alert-warning">
			<strong>Warning!</strong> ${message}
		</div>
	</c:if>
	<script type="text/javascript">
		function showPass1() {
			var x = document.getElementById("pass");
			if (x.type === "password") {
				x.type = "text";
			} else {
				x.type = "password";
			}
		}
		function showPass2() {
			var x = document.getElementById("pass2");
			if (x.type === "password") {
				x.type = "text";
			} else {
				x.type = "password";
			}
		}
		var check = function() {
			var pass1 = document.getElementById('pass');
			var pass2 = document.getElementById('pass2');
			var message = document.getElementById('message');
			var message2 = document.getElementById('message2');
			var goodColor = "#66cc66";
			var badColor = "#ff6666";
			console.log(document.getElementById('pass').value);
			if (pass1.value == pass2.value) {
				message.style.color = goodColor;
				message.innerHTML = 'Matching';
			} else {
				message.style.color = badColor;
				message.innerHTML = 'Not matching';
			}
			if (pass1.value.length > 5) {
				message2.style.color = goodColor;
				message2.innerHTML = "Character number ok!"
			} else {
				message2.style.color = badColor;
				message2.innerHTML = " You have to enter at least 6 digit!"
			}
		}
		function checkPassword() {
			password1 = document.getElementById('pass').value;
			password2 = document.getElementById('pass2').value;
			// If Not same return False.     
			if (password1 != password2) {
				return false;
			}
			if (password1.length < 5) {
				return false;
			}
		}
	</script>
	<div class="container">
		<h1>
			<i class="fa fa-user-plus"></i>Make a new User:
		</h1>
		<form:form action="save/${roleid}" modelAttribute="user" method="POST"
			onSubmit="return checkPassword()" class="ui form" name="myForm">
		
		Role:
		${role}<form:radiobutton path="role" value="${role}" checked="checked" />
			<div class="form-group">
				<label>First Name</label>
				<form:input pattern="[a-zA-Z]*" required="required"
					class="form-control" placeholder="First Name" path="first_name"
					type="text" />
			</div>
			<div class="form-group">
				<label>Last Name</label>
				<form:input pattern="[a-zA-Z]*" required="required"
					class="form-control" placeholder="Last Name" path="last_name"
					type="text" />
			</div>
			<div class="form-group">
				<label>Email</label>
				<form:input required="required" class="form-control"
					placeholder="Enter E-mail" path="email" type="email" />
			</div>
			<div class="form-group">
				<label>Username</label>
				<form:input required="required" class="form-control"
					placeholder="Enter Username" path="username" type="text" />
			</div>
			<div class="form-group">
				<label>Password</label>
				<form:input required="required" class="form-control"
					placeholder="Enter password" id="pass" path="password"
					name="password1" onkeyup='check();' type="password" />
				<span id='message2'></span><br>
				<input type="checkbox" onclick="showPass1()">Show Password 
			</div>
			<div class="form-group">
				<label>Password Confirm</label> <input required="required"
					class="form-control" placeholder="Confirm password" id="pass2"
					name="password2" onkeyup='check();' type="password" /> <span
					id='message'></span><br>
					<input type="checkbox" onclick="showPass2()">Show Password
			</div>
			<label>Department</label>
			<div class="form-group">
				<form:select class="form-control" path="department">
					<form:option value="Geography">Geography</form:option>
					<form:option value="Informatics">Informatics</form:option>
					<form:option value="Dietetics">Dietetics</form:option>
				</form:select>
				<br>
			</div>
			<button class="btn btn-primary" type="submit" id="submitid">
				<i class="fa fa-check"></i>Save
			</button>
		</form:form>
	</div>