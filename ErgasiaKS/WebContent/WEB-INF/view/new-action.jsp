<title>New Action</title>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand/logo -->
		<a class="navbar-brand"> <img
			src="https://upload.wikimedia.org/wikipedia/commons/6/63/Harokopio_University_Logo.png"
			alt="logo" style="width: 40px;">
		</a>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="entry"><i
					class="fa fa-home"></i>Home</a></li>
		</ul>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item"><a class="nav-link" href="<c:url value="/logout" />"><i
					class="fa fa-sign-out"></i>Logout</a></li>
		</ul>
	</nav>
	<ul class="nav nav-tabs" role="tablist">
		<li class="nav-item"><a class="nav-link active" data-toggle="tab"
			href="#home">Manager Actions</a></li>
		<li class="nav-item"><a class="nav-link" data-toggle="tab"
			href="#menu1">Chief Actions</a></li>
		<li class="nav-item"><a class="nav-link" data-toggle="tab"
			href="#menu2">Student Actions</a></li>
	</ul>
	<c:if test="${not empty message}">
		<div class="alert alert-warning">
			<strong>Warning!</strong> ${message}
		</div>
	</c:if>
	<div class="tab-content">
		<div id="home" class="container tab-pane active">
			<br>
			<div class="container">
				<h2>Manager Actions</h2>
				<table class="table table-striped">
					<thead>
						<th>Action_title</th>
						<th>Action_rights</th>
					</thead>
					<tbody>
						<c:forEach var="Action" items="${managersactions}">
							<form:form action="saveAction" modelAttribute="action"
								method="POST" class="ui form">
								<tr>
									<td><form:hidden path="action_title"
											value="${Action.action_title}" />${Action.action_title}</td>
									<td><form:hidden path="action_rights"
											value="${Action.action_rights}" />${Action.action_rights}</td>
									<td><button class="btn btn-primary" type="submit">
											<i class="fa fa-check"></i>Save
										</button>
								</tr>
							</form:form>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<div id="menu1" class="container tab-pane fade">
			<br>
			<div class="container">
				<h2>Chief Actions</h2>
				<table class="table table-striped">
					<thead>
						<th>Action_title</th>
						<th>Action_rights</th>
					</thead>
					<tbody>
						<c:forEach var="Action" items="${chiefsactions}">
							<form:form action="saveAction" modelAttribute="action"
								method="POST" class="ui form">
								<tr>
									<td><form:hidden path="action_title"
											value="${Action.action_title}" />${Action.action_title}</td>
									<td><form:hidden path="action_rights"
											value="${Action.action_rights}" />${Action.action_rights}</td>
									<td><button class="btn btn-primary" type="submit">
											<i class="fa fa-check"></i>Save
										</button>
								</tr>
							</form:form>

						</c:forEach>

					</tbody>
				</table>
			</div>

		</div>
		<div id="menu2" class="container tab-pane fade">
			<br>
			<div class="container">
				<h2>Student Actions</h2>
				<table class="table table-striped">
					<thead>
						<th>Action_title</th>
						<th>Action_rights</th>
					</thead>
					<tbody>
						<c:forEach var="Action" items="${studentsactions}">
							<form:form action="saveAction" modelAttribute="action"
								method="POST" class="ui form">
								<tr>
									<td><form:hidden path="action_title"
											value="${Action.action_title}" />${Action.action_title}</td>
									<td><form:hidden path="action_rights"
											value="${Action.action_rights}" />${Action.action_rights}</td>
									<td><button class="btn btn-primary" type="submit">
											<i class="fa fa-check"></i>Save
										</button>
								</tr>
							</form:form>

						</c:forEach>

					</tbody>
				</table>
			</div>
		</div>
	</div>