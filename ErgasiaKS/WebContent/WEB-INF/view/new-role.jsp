<title>New Role</title>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand/logo -->
		<a class="navbar-brand"> <img
			src="https://upload.wikimedia.org/wikipedia/commons/6/63/Harokopio_University_Logo.png"
			alt="logo" style="width: 40px;">
		</a>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="entry"><i class="fa fa-home"></i>Home</a></li>
		</ul>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item"><a class="nav-link" href="<c:url value="/logout" />"><i class="fa fa-sign-out"></i>Logout</a>
			</li>
		</ul>
	</nav>
	<div class="container">
		<h1>Create a new Role:</h1>

		<form:form action="saveRole" modelAttribute="role" method="POST"
			class="ui form">
			<div class="form-group">
				<label>Role Title</label>
				<form:select class="form-control" path="role_title">
					<form:option value="ROLE_MANAGER">Manager</form:option>
					<form:option value="ROLE_CHIEF">Chief</form:option>
					<form:option value="ROLE_STUDENT">Student</form:option>
				</form:select>
				<br> <label>Role Rights</label>
				<form:select class="form-control" path="role_rights">
					<form:option value="1">ManagerRights</form:option>
					<form:option value="2">ChiefRights</form:option>
					<form:option value="3">StudentRights</form:option>
				</form:select>
				<br>
			</div>
			<button class="btn btn-primary" type="submit"><i class="fa fa-check"></i>Save</button>
		</form:form>
	</div>