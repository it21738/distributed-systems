<title>User Modify</title>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand/logo -->
		<a class="navbar-brand"> <img
			src="https://upload.wikimedia.org/wikipedia/commons/6/63/Harokopio_University_Logo.png"
			alt="logo" style="width: 40px;">
		</a>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link"
				href="/ErgasiaKS/admin/entry"><i class="fa fa-home"></i>Home</a></li>
		</ul>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item"><a class="nav-link"
				href="<c:url value="/logout" />"><i class="fa fa-sign-out"></i>Logout</a></li>
		</ul>
	</nav>
	<c:if test="${not empty message}">
		<div class="alert alert-warning">
			<strong>Warning!</strong> ${message}
		</div>
	</c:if>
	<c:url var="post_url" value="modify/${user.id}" />
	<div class="container">
		<h1>Modify ${user.first_name}:</h1>

		<form:form action="${post_url}" modelAttribute="user" method="POST"
			class="ui form">
			<form:hidden path="old_username" value="${user.username}" />
			<form:hidden path="old_email" value="${user.email}" />
			<form:hidden path="password" value="${user.password}" />
			<form:hidden path="role" value="${user.role}" />
			<div class="form-group">
				<label>First Name</label>
				<form:input pattern="[a-zA-Z]*"
					oninvalid="setCustomValidity('Please enter on alphabets only. ')"
					required="required" class="form-control" placeholder="First Name"
					path="first_name" type="text" />
			</div>
			<div class="form-group">
				<label>Last Name</label>
				<form:input pattern="[a-zA-Z]*"
					oninvalid="setCustomValidity('Please enter on alphabets only. ')"
					required="required" class="form-control" placeholder="Last Name"
					path="last_name" type="text" />
			</div>
			<div class="form-group">
				<label>Email</label>
				<form:input required="required" class="form-control"
					placeholder="Enter E-mail" path="email" type="email" />
			</div>
			<div class="form-group">
				<label>Username</label>
				<form:input required="required" class="form-control"
					placeholder="Enter Username" path="username" type="text" />
			</div>
			<div class="form-group">
				<form:select class="form-control" path="department">
					<form:option value="Geography">Geography</form:option>
					<form:option value="Informatics">Informatics</form:option>
					<form:option value="Dietetics">Dietetics</form:option>
				</form:select>
				<form:hidden path="actions" value="${user.actions}" />
			</div>
			<button class="btn btn-primary" type="submit">
				<i class="fa fa-check"></i>Save
			</button>
		</form:form>
	</div>