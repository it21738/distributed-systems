<title>Chief Entry</title>
</head>
<body>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand/logo -->
		<a class="navbar-brand"> <img
			src="https://upload.wikimedia.org/wikipedia/commons/6/63/Harokopio_University_Logo.png"
			alt="logo" style="width: 40px;">
		</a><span class="navbar-text"> <i class="fa fa-user-circle"></i>Welcome
			Chief
		</span>
		<!-- Links -->
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link"
				href="/ErgasiaKS/chief/updatepos"><i class="fa fa-unlock"></i>Update
					Positions</a></li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link"
				href="/ErgasiaKS/chief/endreq"><i class="fa fa-bell-slash"></i>Expire
					Requests</a></li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link"
				href="/ErgasiaKS/chief/updres"><i class="fa fa-unlock"></i>Update
					Results</a></li>
		</ul>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item"><a class="nav-link" href="<c:url value="/logout" />"><i
					class="fa fa-sign-out"></i>Log out</a></li>

		</ul>
	</nav>

	<c:if test="${not empty message2}">
		<div class="alert alert-warning">
			<strong>Warning!</strong> ${message2}
		</div>
	</c:if>
	<c:if test="${not empty message}">
		<div class="alert alert-success">
			<strong>Success!</strong> ${message}
		</div>
	</c:if>
	<div class="container">
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item"><a class="nav-link active"
				data-toggle="tab" href="#home">Students</a></li>
			<li class="nav-item"><a class="nav-link" data-toggle="tab"
				href="#menu1">Requests</a></li>
		</ul>
		<div class="tab-content">
			<div id="home" class="container tab-pane active">
				<h2>Students to Activate</h2>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>StudentId</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
							<th>Department</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<!-- loop over and print our customers -->
						<c:forEach var="Student" items="${students}">

							<tr>
								<td>${Student.id}</td>
								<td>${Student.first_name}</td>
								<td>${Student.last_name}</td>
								<td>${Student.email}</td>
								<td>${Student.department}</td>
								<td>
									<button type="button" class="btn btn-outline-primary">
										<a
											href="<c:url value="/chief/activate/${Student.id}"></c:url>"><i
											class="fa fa-unlock"></i>Activate</a>
									</button>
							</tr>

						</c:forEach>
					</tbody>
				</table>
			</div>
			<div id="menu1" class="container tab-pane fade">
				<h2>Requests</h2>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Id</th>
							<th>User Id</th>
							<th>Family Income</th>
							<th>Study Brothers</th>
							<th>Comes From</th>
							<th>Study Years</th>
							<th>Free Accomodation Years</th>
							<th>Department</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<!-- loop over and print our customers -->
						<c:forEach var="Request" items="${requests}">

							<tr>
								<td>${Request.req_id}</td>
								<td>${Request.user_id}</td>
								<td>${Request.family_income}</td>
								<td>${Request.no_of_study_bros}</td>
								<td>${Request.comes_from}</td>
								<td>${Request.study_years}</td>
								<td>${Request.accom_years}</td>
								<td>${Request.req_dep}</td>

								<td>
									<button type="button" class="btn btn-outline-primary">
										<a
											href="<c:url value="/chief/testreq/${Request.req_id}"></c:url>"><i
											class="unhide icon"></i>Test Request</a>
									</button>
							</tr>

						</c:forEach>
					</tbody>
				</table>
				<c:if test="${not empty message3}">
					<div class="alert alert-success">
						<strong>Success!</strong> ${message3}
					</div>
				</c:if>
			</div>
		</div>
	</div>