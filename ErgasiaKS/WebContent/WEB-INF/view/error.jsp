<title>${errorMsg}</title>
</head>
<style>
@import
	url('https://fonts.googleapis.com/css?family=Kalam&display=swap');
</style>
<style>
.module {
	display: grid;
	position: relative;
}

.module::before {
	content: "";
	width: 100%;
	height: 100%;
	position: fixed;
	background-image:
		url("https://www.neolaia.gr/wp-content/uploads/2013/01/xarokopeio.jpg");
	background-size: cover;
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
	filter: grayscale(100%);
}

.module-inside {
	position: relative;
}

#h1 {
	font-family: 'Kalam', cursive;
}

.h1 {
	font-weight: bold;
	text-align: center;
	font-family: 'Kalam', cursive;
	color: black;
}
</style>
<body>
	<div class="module">
		<div class="module-inside">
			<div class="shadow-lg p-4">
				<div class="navbar navbar-inverse navbar-fixed-top" id=nav1>
					<a class="navbar-brand"> <img
						src="https://pagecdn.io/repo/10521952be691de3ac6b/wp-content/uploads/2018/03/logo_hua_full.png"
						style="width: 200px;"></a> <span id="h1" class="navbar-text"><font
						size="6"> <c:if test="${role=='[ROLE_ADMIN]'}">
								<button class="btn btn-dark" > <a href="<c:url value="/admin/entry"/>">
									<i class="fa fa-home"></i> Go to my Home page </a>
								</button>
							</c:if> <c:if test="${role=='[ROLE_CHIEF]'}">
								<button class="btn btn-dark"> <a href="<c:url value="/chief/entry"/>">
									<i class="fa fa-home"></i> Go to my Home page</a>
								</button>
							</c:if> <c:if test="${role=='[ROLE_MANAGER]'}">
								<button class="btn btn-dark"> <a href="<c:url value="/manager/entry"/>">
									<i class="fa fa-home"></i> Go to my Home page</a>
								</button>
							</c:if></font> </span>

				</div>
			</div>

			<br> <br>
			<div class="container-sm">
				<div class="h1">
					<h1>${errorMsg}</h1>
					<p>Sorry you cant access this page!</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>