package Intranet.Handlers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HTTPErrorHandler {
	@RequestMapping(value = "/400")
	public String error400(Model model) {
		model.addAttribute("errorMsg", "User error handler 400");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("role", auth.getAuthorities());
		return "error";
	}

	@RequestMapping(value = "/404")
	public String error404(Model model) {
		model.addAttribute("errorMsg", "User error handler 404");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("role", auth.getAuthorities());
		return "error";
	}
	@RequestMapping(value = "/403")
	public String error403(Model model) {
		model.addAttribute("errorMsg", "User error handler 403");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("role", auth.getAuthorities());
		return "error";
	}

	@RequestMapping(value = "/500")
	public String error500(Model model) {
		model.addAttribute("errorMsg", "User error handler 500");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("role", auth.getAuthorities());
		return "error";
	}
}
