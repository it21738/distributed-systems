package Intranet.Service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Intranet.DAO.RoleDao;
import Intranet.entity.Role;
@Service
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleDao roleDAO;
	@Override
	@Transactional
	public List<Role> getRoles() {		
		return roleDAO.getRoles();
	}

	@Override
	@Transactional
	public void saveRole(Role role) {
		roleDAO.saveRole(role);
	}

	@Override
	@Transactional
	public Role getRole(int id) {
		return roleDAO.getRole(id);
	}

	@Override
	@Transactional
	public void updateRole(Role role) {
		roleDAO.updateRole(role);
	}

	@Override
	@Transactional
	public void deleteRole(int id) {
		roleDAO.deleteRole(id);
	}

}
