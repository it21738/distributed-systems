package Intranet.Service;

import java.util.List;

import Intranet.entity.AcceptedRequests;
import Intranet.entity.Department;
import Intranet.entity.RefusedRequests;

public interface TestedRequestsService {
	public List<AcceptedRequests> getAcceptedRequests();
	public void saveAcceptedRequest(AcceptedRequests acceptedreq) ;
	public void deleteAcceptedRequests(int req_id);
	public void updateAcceptedRequest(AcceptedRequests acceptedreq);
	public AcceptedRequests getAcceptedRequest(int req_id);
	public AcceptedRequests getAcceptedRequestByUserId(int user_id);
	public List<AcceptedRequests> getAcceptedRequestsByDepartment(String department);
}
