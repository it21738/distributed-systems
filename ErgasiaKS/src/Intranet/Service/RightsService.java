package Intranet.Service;

import java.util.List;

import Intranet.entity.ChiefRights;
import Intranet.entity.ManagerRights;
import Intranet.entity.StudentRights;

public interface RightsService {
	public List<ManagerRights> getManagerRights();
	 public void saveManagerRights(ManagerRights manager_rights) ;
	 public ManagerRights getManagersRights(int user_id);
	 public void updateManagerRights(ManagerRights manager_rights);
	 public void deleteManagerRights(int user_id);
	 
	 public List<ChiefRights> getChiefRights();
	 public void saveChiefRights(ChiefRights chief_rights) ;
	 public ChiefRights getChiefsRights(int user_id);
	 public void updateChiefRights(ChiefRights chief_rights);
	 public void deleteChiefRights(int user_id);
	 
	 public List<StudentRights> getStudentRights();
	 public void saveStudentRights(StudentRights student_rights) ;
	 public StudentRights getStudentsRights(int user_id);
	 public void updateStudentRights(StudentRights student_rights);
	 public void deleteStudentRights(int user_id);
}
