package Intranet.Service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Intranet.DAO.DepartmentDao;
import Intranet.entity.Department;
@Service
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentDao departmentDAO;
	@Override
	@Transactional
	public List<Department> getDepartments() {
		return departmentDAO.getDepartments();
	}

	@Override
	@Transactional
	public void saveDepartment(Department department) {
		departmentDAO.saveDepartment(department);
	}

	@Override
	@Transactional
	public Department getDepartment(int dept_id) {
		return departmentDAO.getDepartment(dept_id);
	}

	@Override
	@Transactional
	public void updateDepartment(Department department) {
		departmentDAO.updateDepartment(department);
	}

	@Override
	@Transactional
	public void deleteDepartment(int dept_id) {
		departmentDAO.deleteDepartment(dept_id);
	}

}
