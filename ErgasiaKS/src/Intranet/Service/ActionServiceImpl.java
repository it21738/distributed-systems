package Intranet.Service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Intranet.DAO.ActionDao;
import Intranet.DAO.RoleDao;
import Intranet.entity.Action;
@Service
public class ActionServiceImpl implements ActionService {
	@Autowired
	private ActionDao actionDAO;
	@Override
	@Transactional
	public List<Action> getActions() {
		return actionDAO.getActions();

	}

	@Override
	@Transactional
	public void saveAction(Action action) {
		actionDAO.saveAction(action);
	}

	@Override
	@Transactional
	public void deleteAction(int id) {
		actionDAO.deleteAction(id);
	}

	@Override
	public void updateAction(Action action) {
		actionDAO.updateAction(action);
	}

	@Override
	public Action getAction(int id) {
		return actionDAO.getAction(id);
	}		
}


