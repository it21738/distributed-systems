package Intranet.Service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Intranet.DAO.DepartmentDao;
import Intranet.DAO.UniversityDao;
import Intranet.entity.University;
@Service
public class UniversityServiceImpl implements UniversityService {

	@Autowired
	private UniversityDao universityDAO;
	@Override
	@Transactional
	public List<University> getUniversities() {
		return universityDAO.getUniversities();
	}

	@Override
	@Transactional
	public void saveUniversity(University university) {
		 universityDAO.saveUniversity(university);
	}

	@Override
	@Transactional
	public University getUniversity(int un_id) {
		return universityDAO.getUniversity(un_id);
	}

	@Override
	@Transactional
	public void updateUniversity(University university) {
		universityDAO.updateUniversity(university);
	}

	@Override
	@Transactional
	public void deleteUniversity(int un_id) {
		universityDAO.deleteUniversity(un_id);
	}

}
