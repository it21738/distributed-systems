package Intranet.Service;

import java.util.List;

import Intranet.entity.Role;

public interface RoleService {
	public List<Role> getRoles();
	 public void saveRole(Role role) ;
	 public Role getRole(int id);
	 public void updateRole(Role role);
	 public void deleteRole(int id);
}
