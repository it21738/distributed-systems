package Intranet.Service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Intranet.DAO.UserDao;
import Intranet.entity.Action;
import Intranet.entity.User;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDAO;

	@Override
	@Transactional
	public List<User> getUsers() {
		return userDAO.getUsers();
	}

	@Override
	@Transactional
	public void saveUser(User user) {
		userDAO.saveUser(user);
	}

	@Override
	@Transactional
	public User getUser(int id) {
		return userDAO.getUser(id);
	}

	@Override
	@Transactional
	public void deleteUser(int id) {
		userDAO.deleteUser(id);
	}

	@Override
	@Transactional
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		userDAO.updateUser(user);
	}
	@Override
	@Transactional
	public List<User> getUsersbyRole(String role, String department) {
		return userDAO.getUsersbyRole(role, department);
	}

	@Override
	@Transactional
	public User getStudent(String username) {
		// TODO Auto-generated method stub
		return userDAO.getStudent(username);
	}

	@Override
	@Transactional
	public User getUserbyUsername(String username) {
		return userDAO.getUserbyUsername(username);
	}


	

	

}
