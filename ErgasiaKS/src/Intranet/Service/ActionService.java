package Intranet.Service;

import java.util.List;

import Intranet.entity.Action;

public interface ActionService {
	public List<Action> getActions();

	public void saveAction(Action action);
	
	public void deleteAction(int id);
	public void updateAction(Action action);
	
	public Action getAction(int id);
}
