package Intranet.Service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Intranet.DAO.RightsDao;
import Intranet.entity.ChiefRights;
import Intranet.entity.ManagerRights;
import Intranet.entity.StudentRights;
@Service
public class RightsServiceImpl implements RightsService {

	@Autowired
	private RightsDao rightsDAO;
	@Override
	@Transactional
	public List<ManagerRights> getManagerRights() {
		// TODO Auto-generated method stub
		return rightsDAO.getManagerRights();
	}
	@Transactional
	@Override
	public void saveManagerRights(ManagerRights manager_rights) {
		rightsDAO.saveManagerRights(manager_rights);
	}
	@Transactional
	@Override
	public ManagerRights getManagersRights(int user_id) {
		return rightsDAO.getManagersRights(user_id);
	}
	@Transactional
	@Override
	public void updateManagerRights(ManagerRights manager_rights) {
		rightsDAO.updateManagerRights(manager_rights);
	}
	@Transactional
	@Override
	public void deleteManagerRights(int user_id) {
		rightsDAO.deleteManagerRights(user_id);
	}
	@Transactional
	@Override
	public List<ChiefRights> getChiefRights() {
		return rightsDAO.getChiefRights();
	}
	@Transactional
	@Override
	public void saveChiefRights(ChiefRights chief_rights) {
		rightsDAO.saveChiefRights(chief_rights);
	}
	@Transactional
	@Override
	public ChiefRights getChiefsRights(int user_id) {
		// TODO Auto-generated method stub
		return rightsDAO.getChiefsRights(user_id);
	}
	@Transactional
	@Override
	public void updateChiefRights(ChiefRights chief_rights) {
		// TODO Auto-generated method stub
		rightsDAO.updateChiefRights(chief_rights);
	}
	@Transactional
	@Override
	public void deleteChiefRights(int user_id) {
		// TODO Auto-generated method stub
		rightsDAO.deleteChiefRights(user_id);
	}
	@Transactional
	@Override
	public List<StudentRights> getStudentRights() {
		return rightsDAO.getStudentRights();
	}
	@Transactional
	@Override
	public void saveStudentRights(StudentRights student_rights) {
		rightsDAO.saveStudentRights(student_rights);
	}
	@Transactional
	@Override
	public StudentRights getStudentsRights(int user_id) {
		return rightsDAO.getStudentsRights(user_id);
	}
	@Transactional
	@Override
	public void updateStudentRights(StudentRights student_rights) {
		rightsDAO.updateStudentRights(student_rights);
	}
	@Transactional
	@Override
	public void deleteStudentRights(int user_id) {
		rightsDAO.deleteStudentRights(user_id);
	}

}
