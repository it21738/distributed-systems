package Intranet.Service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Intranet.DAO.TestedRequestsDao;
import Intranet.entity.AcceptedRequests;
import Intranet.entity.RefusedRequests;
@Service
public class TestedRequestsServiceImpl implements TestedRequestsService {
	@Autowired
	private TestedRequestsDao testedrequestsDAO;
	@Transactional
	@Override
	public List<AcceptedRequests> getAcceptedRequests() {
		// TODO Auto-generated method stub
		return testedrequestsDAO.getAcceptedRequests();
	}
	@Transactional
	@Override
	public void saveAcceptedRequest(AcceptedRequests acceptedreq) {
		testedrequestsDAO.saveAcceptedRequest(acceptedreq);
	}
	@Transactional
	@Override
	public void deleteAcceptedRequests(int req_id) {
		testedrequestsDAO.deleteAcceptedRequests(req_id);
	}
	@Transactional
	@Override
	public void updateAcceptedRequest(AcceptedRequests acceptedreq) {
		testedrequestsDAO.updateAcceptedRequest(acceptedreq);
	}
	@Transactional
	@Override
	public AcceptedRequests getAcceptedRequest(int req_id) {
		// TODO Auto-generated method stub
		return testedrequestsDAO.getAcceptedRequest(req_id);
	}
	@Transactional
	@Override
	public List<AcceptedRequests> getAcceptedRequestsByDepartment(String department) {
		// TODO Auto-generated method stub
		return testedrequestsDAO.getAcceptedRequestsByDepartment(department);
	}
	@Transactional
	@Override
	public AcceptedRequests getAcceptedRequestByUserId(int user_id) {
		return testedrequestsDAO.getAcceptedRequestByUserId(user_id);
	}

}
