package Intranet.Service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Intranet.DAO.DepartmentDao;
import Intranet.DAO.RequestDao;
import Intranet.entity.Request;
@Service
public class RequestServiceImpl implements RequestService {

	@Autowired
	private RequestDao requestDAO;
	@Override
	@Transactional
	public List<Request> getRequests() {
		return requestDAO.getRequests();
	}

	@Override
	@Transactional
	public void saveRequest(Request request) {
		requestDAO.saveRequest(request);
	}

	@Override
	@Transactional
	public Request getRequest(int req_id) {
		return requestDAO.getRequest(req_id);
	}

	@Override
	@Transactional
	public void updateRequest(Request request) {
		requestDAO.updateRequest(request);
	}

	@Override
	@Transactional
	public void deleteRequest(int req_id) {
		requestDAO.deleteRequest(req_id);
	}

	@Override
	@Transactional
	public List<Request> getRequestsByDep(String department) {
		return requestDAO.getRequestsByDep(department);
	}

	@Override
	@Transactional
	public Request getRequestByUserId(int user_id) {
		return requestDAO.getRequestByUserId(user_id);
	}

}
