package Intranet.Service;

import java.util.List;

import Intranet.entity.Action;
import Intranet.entity.User;

public interface UserService {
	public List<User> getUsers();

	public void saveUser(User user);

	public User getUser(int id);

	 public User getUserbyUsername(String username);

	public void updateUser(User user);

	public void deleteUser(int id);
	
	 public List<User> getUsersbyRole(String role,String department);

	 public User getStudent(String username);


}
