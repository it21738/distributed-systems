package Intranet.Security;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@Configuration
public class AppSecurityConfig extends WebSecurityConfigurerAdapter  {
	 @Autowired
	   DataSource dataSource;
	 private AuthenticationSuccessHandler authenticationSuccessHandler;
	 
	    @Autowired
	    public  void WebSecurityConfig(AuthenticationSuccessHandler authenticationSuccessHandler) {
	        this.authenticationSuccessHandler = authenticationSuccessHandler;
	    }
	   @Override
	   protected void configure(AuthenticationManagerBuilder auth) throws Exception {

	           auth.jdbcAuthentication().dataSource(dataSource).passwordEncoder(passwordEncoder())
	                           .usersByUsernameQuery("select username,password,enabled from User where username=?")
	                           .authoritiesByUsernameQuery("select username,role from User where username=?");

	   }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // For example: Use only Http Basic and not form login.
        http
                .authorizeRequests()
                .antMatchers("/manager/**").hasRole("MANAGER")
                .antMatchers("/chief/**").hasRole("CHIEF")
                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/login").permitAll()
                .loginProcessingUrl("/login")
                .successHandler(authenticationSuccessHandler)
                .failureUrl("/login?error=true")
                .permitAll()
            .and()
                .logout()
                .logoutSuccessUrl("/login?logout=true")
                .invalidateHttpSession(true)
                .permitAll()
                .and()
        	    .exceptionHandling().accessDeniedPage("/403")
            .and()
                .csrf()
                .disable();
            
    }
    @Bean
    public PasswordEncoder passwordEncoder() {
            PasswordEncoder encoder = new BCryptPasswordEncoder();
            return encoder;
    }
    @Override
    public void configure(WebSecurity web) throws Exception {
            web.ignoring().antMatchers("/api/**");

    }
}


