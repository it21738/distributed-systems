package Intranet.Controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import Intranet.Service.*;
import Intranet.entity.*;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private UserService userservice;
	@Autowired
	private RoleService roleservice;
	@Autowired
	private ActionService actionservice;
	@Autowired
	private RightsService rightsservice;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@GetMapping("/entry")
	public String showAdmin(Model model) {
		List<User> users = userservice.getUsers();
		List<Action> actions = actionservice.getActions();
		List<Role> roles = roleservice.getRoles();
		System.out.println(actions);
		model.addAttribute("actions", actions);
		model.addAttribute("users", users);
		model.addAttribute("roles", roles);
		return "admin-entry";
	}

	@GetMapping("/NewRole")
	public String MakeNewRole(Model model) {
		Role role = new Role();
		model.addAttribute("role", role);
		return "new-role";
	}

	@PostMapping("/saveRole")
	public String SaveRole(@ModelAttribute("role") Role role) {
		System.out.println(role.getRole_rights());
		roleservice.saveRole(role);
		return "redirect:/admin/entry";
	}

	@GetMapping("/deleterole/{id}")
	public String deleteRole(Model model, @PathVariable("id") int id) {
		roleservice.deleteRole(id);
		return "redirect:/admin/entry";
	}

	@GetMapping("/role/get/{id}")
	public String getRole(Model model, @PathVariable("id") int id) {
		Role role = roleservice.getRole(id);
		model.addAttribute("role", role);
		System.out.println(role);
		return "role-modify";
	}

	@PostMapping("/role/get/modifyrole/{id}")
	public String ModifyRole(@PathVariable("id") int id, @ModelAttribute("role") Role role) {
		System.out.println(role);
		role.setRole_id(id);
		roleservice.updateRole(role);
		return "redirect:/admin/entry";
	}

	@GetMapping("/NewAction")
	public String MakeNewActions(Model model) {
		Action ListAct = new Action();
		ArrayList<Action> managersactions = ListAct.createManagersActions();
		ArrayList<Action> chiefsactions = ListAct.createChiefsActions();
		ArrayList<Action> studentsactions = ListAct.createStudentssActions();
		model.addAttribute("managersactions", managersactions);
		model.addAttribute("chiefsactions", chiefsactions);
		model.addAttribute("studentsactions", studentsactions);
		Action action = new Action();
		model.addAttribute("action", action);
		return "new-action";
	}

	@PostMapping("/saveAction")
	public String SaveAction(Model model, @ModelAttribute("action") Action action, RedirectAttributes attributes) {
		System.out.print(action);
		List<Action> actions = actionservice.getActions();
		for (Action action1 : actions) {
			if (action1.getAction_title().equals(action.getAction_title())
					&& action1.getAction_rights() == action.getAction_rights()) {
				attributes.addFlashAttribute("message", "You have already created this Action!");
				// return "new-action";
				return "redirect:/admin/NewAction";
			}
		}
		actionservice.saveAction(action);
		return "redirect:/admin/entry";

	}

	@GetMapping("/deleteAction/{id}")
	public String deleteAction(Model model, @PathVariable("id") int id) {
		actionservice.deleteAction(id);
		return "redirect:/admin/entry";
	}

	@GetMapping("/NewUser/{id}")
	public String MakeNewUsers(Model model, @PathVariable("id") int id, RedirectAttributes attributes) {
		Role role = roleservice.getRole(id);
		model.addAttribute("role", role.getRole_title());
		model.addAttribute("roleid", role.getRole_id());
		int managers = 0;
		List<User> users = userservice.getUsers();
		for (User user1 : users) {
			if (user1.getRole().equals("ROLE_CHIEF") && role.getRole_title().equals("ROLE_CHIEF")) {
				attributes.addFlashAttribute("message", "You cant create another chief!");
				return "redirect:/admin/entry";
			}
			if (user1.getRole().equals("ROLE_MANAGER")) {
				managers += 1;
			}
		}
		if (managers == 2 && role.getRole_title().equals("ROLE_MANAGER")) {
			attributes.addFlashAttribute("message", "You cant create another manager!");
			return "redirect:/admin/entry";
		}
		User user = new User();
		model.addAttribute("user", user);
		return "new-user";
	}

	@PostMapping("NewUser/save/{id}")
	public String SaveUserr(@ModelAttribute("user") User user, @PathVariable("id") int id, Model model,
			RedirectAttributes attributes) {
		// String jsp=null;
		List<User> users = userservice.getUsers();
		for (User user1 : users) {
			if ((user1.getRole().equals("Manager") && user.getRole().equals("ROLE_MANAGER")
					|| user1.getRole().equals("ROLE_CHIEF") && user.getRole().equals("ROLE_MANAGER")
					|| user1.getRole().equals("ROLE_MANAGER") && user.getRole().equals("ROLE_CHIEF"))
					&& user1.getDepartment().equals(user.getDepartment())) {
				attributes.addFlashAttribute("message", "You cant create another manager for this department!");
				return "redirect:/admin/NewUser/{id}";
			} else if (user1.getEmail().equals(user.getEmail())) {
				attributes.addFlashAttribute("message", "Please enter another email.This email is used!");
				return "redirect:/admin/NewUser/{id}";
			} else if (user1.getUsername().equals(user.getUsername())) {
				attributes.addFlashAttribute("message", "Please enter another username.This username is used!");
				return "redirect:/admin/NewUser/{id}";
			}
		}
		Role role = roleservice.getRole(id);
		int action_rights = role.getRole_rights();
		ArrayList<String> actions1 = new ArrayList<String>();
		List<Action> actions = actionservice.getActions();
		for (int i = 0; i < actions.size(); i++) {
			Action action = actions.get(i);
			if (action.getAction_rights() == (action_rights)) {
				actions1.add(action.getAction_title());
			}
		}
		user.setActions(actions1);
		System.out.println(user);
		String hash = passwordEncoder.encode(user.getPassword());
		user.setPassword(hash);
		userservice.saveUser(user);
		if (role.getRole_rights() == 1) {
			ManagerRights manager_right = new ManagerRights(user.getId(), false, false);
			rightsservice.saveManagerRights(manager_right);
			System.out.println(rightsservice.getManagersRights(user.getId()));
		} else if (role.getRole_rights() == 2) {
			ChiefRights chief_right = new ChiefRights(user.getId(), false, false, false, false);
			rightsservice.saveChiefRights(chief_right);
		} else {
			StudentRights student_right = new StudentRights(user.getId(), false, false, false, false);
			rightsservice.saveStudentRights(student_right);
		}
		return "redirect:/admin/entry";
	}

	@GetMapping("/{id}")
	public String deleteUser(Model model, @PathVariable("id") int id) {
		userservice.deleteUser(id);
		if (rightsservice.getManagersRights(id) != null) {
			rightsservice.deleteManagerRights(id);
		} else if (rightsservice.getChiefsRights(id) != null) {
			rightsservice.deleteChiefRights(id);
		} else {
			rightsservice.deleteStudentRights(id);
		}
		return "redirect:/admin/entry";
	}

	@GetMapping("/get/{id}")
	public String getUser(Model model, @PathVariable("id") int id) {
		User user = userservice.getUser(id);
		List<Role> roles = roleservice.getRoles();
		model.addAttribute("user", user);
		model.addAttribute("roles", roles);
		return "user-modify";
	}

	@PostMapping("/get/modify/{id}")
	public String ModifyUser(@ModelAttribute("user") User user, RedirectAttributes attributes) {
		List<User> users = userservice.getUsers();
		if (!user.getUsername().equals(user.getOld_username())) {
			for (User user1 : users) {
				if (user1.getUsername().equals(user.getUsername())) {
					attributes.addFlashAttribute("message", "Please enter another username.This username is used!");
					return "redirect:/admin/get/{id}";
				}
			}
		}else if(!user.getEmail().equals(user.getOld_email())){
			for (User user1 : users) {
				if (user1.getEmail().equals(user.getEmail())) {
					attributes.addFlashAttribute("message", "Please enter another email.This email is used!");
					return "redirect:/admin/get/{id}";
				}
			}
		}
		System.out.println(user);
		userservice.updateUser(user);
		return "redirect:/admin/entry";
	}

	@GetMapping("/SeeRights")
	public String ShowRights(Model model) {
		List<ManagerRights> managerrights = rightsservice.getManagerRights();
		List<ChiefRights> chiefrights = rightsservice.getChiefRights();
		List<StudentRights> studentrights = rightsservice.getStudentRights();
		model.addAttribute("managerrights", managerrights);
		model.addAttribute("chiefrights", chiefrights);
		model.addAttribute("studentrights", studentrights);
		return "see-rights";
	}
}
