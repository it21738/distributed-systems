package Intranet.Controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import Intranet.Service.DepartmentService;
import Intranet.Service.RequestService;
import Intranet.Service.RightsService;
import Intranet.Service.TestedRequestsService;
import Intranet.Service.UniversityService;
import Intranet.Service.UserService;
import Intranet.entity.AcceptedRequests;
import Intranet.entity.Action;
import Intranet.entity.RefusedRequests;
import Intranet.entity.Request;
import Intranet.entity.StudentRights;
import Intranet.entity.User;

@Controller
@RequestMapping("/manager")
public class ManagerController {
	
	@Autowired
    private UserService userservice;
    @Autowired
	private UniversityService unservice;
	@Autowired
	private RightsService rightsservice;
	@Autowired
	private DepartmentService depservice;
	@Autowired
	private RequestService requestservice;
	@Autowired
	private TestedRequestsService testedrequestsservice;
	User manager = new User();
	@GetMapping("/entry")
	public String checkManager(HttpServletRequest request,Model model,Principal principal) {
		String username=principal.getName();
		manager=userservice.getUserbyUsername(username);
		System.out.println(manager);
		List<User> students = new ArrayList<>();
		if (rightsservice.getManagersRights(manager.getId()).isActivate_student() == true) {
			List<User> students1 = userservice.getUsersbyRole("ROLE_STUDENT", manager.getDepartment());
			for (User user1 : students1) {
				if (rightsservice.getStudentsRights(user1.getId()).isCreate_req() == false && requestservice.getRequestByUserId(user1.getId())==null && testedrequestsservice.getAcceptedRequestByUserId(user1.getId())==null) {
					students.add(user1);
				}
			}
		}
		if (rightsservice.getManagersRights(manager.getId()).isHandle_request() == true) {
			List<Request> requests = requestservice.getRequestsByDep(manager.getDepartment());
			model.addAttribute("requests", requests);

		}
		model.addAttribute("students", students);
		model.addAttribute("user", manager);
		return "manager-entry";
	}
	@GetMapping("/activate/{id}")
	public String ActivateStudent(HttpServletRequest request, Model model, @PathVariable("id") int id) {
		StudentRights st_rights = rightsservice.getStudentsRights(id);
		st_rights.setCreate_req(true);
		rightsservice.updateStudentRights(st_rights);
		return "redirect:/manager/entry";
	}
	@GetMapping("/testreq/{id}")
	public String CheckRequest(HttpServletRequest request, Model model, @PathVariable("id") int id,RedirectAttributes attributes) {
		Request req = requestservice.getRequest(id);
		StudentRights studentrights=rightsservice.getStudentsRights(req.getUser_id());
		studentrights.setView_rank(true);
		studentrights.setCreate_req(false);
		rightsservice.updateStudentRights(studentrights);
		if (req.getComes_from().isEmpty() || req.getComes_from().equals(unservice.getUniversity(1).getCity())
				|| req.getStudy_years() > 4) {
			attributes.addFlashAttribute("message", "Request sented to Refused requests succesfully!");
			requestservice.deleteRequest(req.getReq_id());
		} else {
			int points = req.GetPointsOfRequest(req);
			AcceptedRequests acc_req=new AcceptedRequests(req.getReq_id(),req.getUser_id(),points,req.getReq_dep());
			testedrequestsservice.saveAcceptedRequest(acc_req);
			attributes.addFlashAttribute("message", "Request sented to Accepted requests succesfully!");
			requestservice.deleteRequest(req.getReq_id());
		}
		return "redirect:/manager/entry";
	}
}
