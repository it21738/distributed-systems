package Intranet.Controller;

import java.security.Principal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

//import org.apache.coyote.Request;
import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import Intranet.DAO.TestedRequestsDao;
import Intranet.Service.DepartmentService;
import Intranet.Service.RequestService;
import Intranet.Service.RightsService;
import Intranet.Service.TestedRequestsService;
import Intranet.Service.UniversityService;
import Intranet.Service.UserService;
import Intranet.entity.*;

@Controller
@RequestMapping("/chief")
public class ChiefController {
	@Autowired
	private UniversityService unservice;
	@Autowired
	private RightsService rightsservice;
	@Autowired
	private DepartmentService depservice;
	@Autowired
	private UserService userservice;
	@Autowired
	private RequestService requestservice;
	@Autowired
	private TestedRequestsService testedrequestsservice;
	public User chief = new User();

	@GetMapping("/entry")
	public String checkManager(HttpServletRequest request, Model model, Principal principal) {
		String username = principal.getName();
		chief = userservice.getUserbyUsername(username);
		System.out.println(chief);
		List<User> students = new ArrayList<>();
		if (rightsservice.getChiefsRights(chief.getId()).getActivate_student() == true) {
			List<User> students1 = userservice.getUsersbyRole("ROLE_STUDENT", chief.getDepartment());
			for (User user1 : students1) {
				if (rightsservice.getStudentsRights(user1.getId()).isCreate_req() == false
						&& requestservice.getRequestByUserId(user1.getId()) == null
						&& testedrequestsservice.getAcceptedRequestByUserId(user1.getId()) == null) {
					students.add(user1);
				}
			}
		}
		if (rightsservice.getChiefsRights(chief.getId()).getHandle_request() == true) {
			List<Request> requests = requestservice.getRequestsByDep(chief.getDepartment());
			model.addAttribute("requests", requests);

		}
		model.addAttribute("students", students);
		model.addAttribute("user", chief);
		return "chief-entry";
	}

	@GetMapping("/activate/{id}")
	public String ActivateStudent(HttpServletRequest request, Model model, @PathVariable("id") int id) {
		StudentRights st_rights = rightsservice.getStudentsRights(id);
		st_rights.setCreate_req(true);
		rightsservice.updateStudentRights(st_rights);
		return "redirect:/chief/entry";
	}

	@GetMapping("/testreq/{id}")
	public String CheckRequest(HttpServletRequest request, Model model, @PathVariable("id") int id,
			RedirectAttributes attributes) {
		Request req = requestservice.getRequest(id);
		StudentRights studentrights = rightsservice.getStudentsRights(req.getUser_id());
		studentrights.setView_rank(true);
		studentrights.setCreate_req(false);
		rightsservice.updateStudentRights(studentrights);
		if (req.getComes_from().isEmpty() || req.getComes_from().equals(unservice.getUniversity(1).getCity())
				|| req.getStudy_years() > 4) {
			attributes.addFlashAttribute("message", "Request sented to Refused requests!");
			requestservice.deleteRequest(req.getReq_id());

		} else {
			int points = req.GetPointsOfRequest(req);
			AcceptedRequests acc_req = new AcceptedRequests(req.getReq_id(), req.getUser_id(), points,
					req.getReq_dep());
			testedrequestsservice.saveAcceptedRequest(acc_req);
			attributes.addFlashAttribute("message", "Request sented to Accepted requests!");
			requestservice.deleteRequest(req.getReq_id());
		}
		return "redirect:/chief/entry";
	}

	@GetMapping("/updatepos")
	public String UpdatePositions(HttpServletRequest request, Model model, RedirectAttributes attributes) {
		int year1 = unservice.getUniversity(1).getYear();
		int year = Calendar.getInstance().get(Calendar.YEAR);
		if (year != year1) {
			ChiefRights chief_rights = rightsservice.getChiefsRights(chief.getId());
			chief_rights.setUpdate_pos(true);
			rightsservice.updateChiefRights(chief_rights);
			University xarokopeio = unservice.getUniversity(1);
			xarokopeio.setYear(year);
			unservice.updateUniversity(xarokopeio);
			attributes.addFlashAttribute("message", "Positions updated succesfully!");
		} else {
			ChiefRights chief_rights = rightsservice.getChiefsRights(chief.getId());
			chief_rights.setUpdate_pos(false);
			rightsservice.updateChiefRights(chief_rights);
			attributes.addFlashAttribute("message2", "You have already updated the positions!");
		}
		if (rightsservice.getChiefsRights(chief.getId()).getUpdate_pos() == true) {
			for (Department department : depservice.getDepartments()) {
				department.setFree_pos(40);
				department.setDept_pos(60);
				depservice.updateDepartment(department);

			}
			for (User user : userservice.getUsers()) {
				if (user.getRole().equals("ROLE_MANAGER")) {
					ManagerRights manager_rights = rightsservice.getManagersRights(user.getId());
					manager_rights.setActivate_student(true);
					manager_rights.setHandle_request(true);
					rightsservice.updateManagerRights(manager_rights);
				} else if (user.getRole().equals("ROLE_CHIEF")) {
					ChiefRights chief_rights = rightsservice.getChiefsRights(user.getId());
					chief_rights.setActivate_student(true);
					chief_rights.setHandle_request(true);
					rightsservice.updateChiefRights(chief_rights);
				} else if (user.getRole().equals("ROLE_STUDENT")) {
					StudentRights student_rights = rightsservice.getStudentsRights(user.getId());
					if (student_rights.isCreate_req() == true) {
						student_rights.setCreate_req(false);
						rightsservice.updateStudentRights(student_rights);
					}

				}
			}
		}

		return "redirect:/chief/entry";
	}

	@GetMapping("/endreq")
	public String expireRequests(HttpServletRequest request, Model model, RedirectAttributes attributes) {
		if (rightsservice.getChiefsRights(chief.getId()).getActivate_student() == false) {
			attributes.addFlashAttribute("message2", "You cant do this now!");
		} else {

			for (ChiefRights chiefrights : rightsservice.getChiefRights()) {
				chiefrights.setExp_reqs(true);
				chiefrights.setActivate_student(false);
				chiefrights.setHandle_request(false);
				rightsservice.updateChiefRights(chiefrights);
			}
			for (ManagerRights managerrights : rightsservice.getManagerRights()) {
				managerrights.setActivate_student(false);
				managerrights.setHandle_request(false);
				rightsservice.updateManagerRights(managerrights);
			}
			attributes.addFlashAttribute("message", "Requests expired succesfully!");
		}
		return "redirect:/chief/entry";
	}

	@GetMapping("/updres")
	public String updateRequestsResults(HttpServletRequest request, Model model, RedirectAttributes attributes) {
		if (rightsservice.getChiefsRights(chief.getId()).isExp_reqs() == true
				&& requestservice.getRequests().isEmpty()) {
			Department Geography = depservice.getDepartment(1);
			Department Informatics = depservice.getDepartment(2);
			Department Dietetics = depservice.getDepartment(3);
			List<AcceptedRequests> accepted_geo_requests = testedrequestsservice
					.getAcceptedRequestsByDepartment("Geography");
			List<AcceptedRequests> accepted_inf_requests = testedrequestsservice
					.getAcceptedRequestsByDepartment("Informatics");
			List<AcceptedRequests> accepted_diet_requests = testedrequestsservice
					.getAcceptedRequestsByDepartment("Dietetics");
			if (testedrequestsservice.getAcceptedRequestsByDepartment("Geography").size() >= 40) {
				Geography.setFree_pos(0);
				Geography.setDept_pos(100);
				depservice.updateDepartment(Geography);
				for (int i = 0; i < accepted_geo_requests.size(); i++) {
					if (i > 39) {
						AcceptedRequests go_to_ref = accepted_geo_requests.get(i);
						testedrequestsservice.deleteAcceptedRequests(go_to_ref.getReq_id());
					}
				}

			} else {
				Geography.setFree_pos(40 - accepted_geo_requests.size());
				Geography.setDept_pos(60 + accepted_geo_requests.size());
				depservice.updateDepartment(Geography);

			}
			if (accepted_inf_requests.size() >= 40) {
				Informatics.setFree_pos(0);
				Informatics.setDept_pos(100);
				depservice.updateDepartment(Informatics);
				for (int i = 0; i < accepted_inf_requests.size(); i++) {
					if (i > 39) {
						AcceptedRequests go_to_ref = accepted_inf_requests.get(i);
						testedrequestsservice.deleteAcceptedRequests(go_to_ref.getReq_id());
					}
				}

			} else {
				Informatics.setFree_pos(40 - accepted_inf_requests.size());
				Informatics.setDept_pos(60 + accepted_inf_requests.size());
				depservice.updateDepartment(Informatics);

			}
			if (accepted_diet_requests.size() >= 40) {
				Dietetics.setFree_pos(0);
				Dietetics.setDept_pos(100);
				depservice.updateDepartment(Dietetics);
				for (int i = 0; i < accepted_diet_requests.size(); i++) {
					if (i > 39) {
						AcceptedRequests go_to_ref = accepted_diet_requests.get(i);
						testedrequestsservice.deleteAcceptedRequests(go_to_ref.getReq_id());
					}
				}
			} else {
				Dietetics.setFree_pos(40 - accepted_diet_requests.size());
				Dietetics.setDept_pos(60 + accepted_diet_requests.size());
				depservice.updateDepartment(Dietetics);
			}
			for (StudentRights studentrights : rightsservice.getStudentRights()) {
				studentrights.setView_res(true);
				rightsservice.updateStudentRights(studentrights);
			}
			attributes.addFlashAttribute("message", "Requests results updated succesfully!");
		} else {

			attributes.addFlashAttribute("message2", "You cant do this now!");
		}
		return "redirect:/chief/entry";
	}

}
