package Intranet.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import Intranet.Service.DepartmentService;
import Intranet.Service.UniversityService;
import Intranet.Service.UserService;
import Intranet.entity.Department;
import Intranet.entity.University;
import Intranet.entity.User;

@Controller
public class HomeController {
	@Autowired
	private UniversityService unservice;
	@Autowired
	private DepartmentService depservice;
	@Autowired
	private UserService userservice;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showMyPage(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, Model model) {
		String errorMessge = null;
		if (error != null) {
			errorMessge = "Username or Password is incorrect !!";
		}
		if (logout != null) {
			errorMessge = "You have been successfully logged out !!";
		}
		model.addAttribute("errorMessge", errorMessge);
		if (userservice.getUsers().isEmpty()) {
			String hash = new BCryptPasswordEncoder().encode("admin2019");
			User admin = new User("ROLE_ADMIN", "Admin", "Admin", "admin", "admin@gmail.com", hash, "", null);
			userservice.saveUser(admin);
		}
		if (unservice.getUniversities().isEmpty()) {
			University HUA = new University(1, "Hua", "Xarokopeio@hua.gr", 2018, "Athens", 300);
			unservice.saveUniversity(HUA);
		}
		if (depservice.getDepartments().isEmpty()) {
			Department Geography = new Department(1, "Geography", 0, 0);
			depservice.saveDepartment(Geography);
			Department Informatics = new Department(2, "Informatics", 0, 0);
			depservice.saveDepartment(Informatics);
			Department Dietetics = new Department(3, "Dietetics", 0, 0);
			depservice.saveDepartment(Dietetics);
		}
		return "login";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout=true";
	}
}
