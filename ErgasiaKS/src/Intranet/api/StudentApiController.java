package Intranet.api;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import Intranet.Service.RequestService;
import Intranet.Service.RightsService;
import Intranet.Service.TestedRequestsService;
import Intranet.Service.UserService;
import Intranet.entity.AcceptedRequests;
import Intranet.entity.Request;
import Intranet.entity.StudentRights;
import Intranet.entity.User;

@RestController
@RequestMapping("/api/student")
public class StudentApiController {
	@Autowired
	private UserService userservice;
	@Autowired
	private RequestService requestservice;
	@Autowired
	private TestedRequestsService testedrequestsservice;
	@Autowired
	private RightsService rightsservice;

	@RequestMapping(value = "/getStudent", method = RequestMethod.POST, produces = { "application/json",
			"application/xml" })
	public User getUser(@RequestParam("username") String username, @RequestParam("password") String password) {
		BCryptPasswordEncoder b = new BCryptPasswordEncoder();
		System.out.println(password);
		if (userservice.getStudent(username) != null) {
			User student = userservice.getStudent(username);
			if (b.matches(password, student.getPassword())) {
				return student;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST, produces = { "application/json",
			"application/xml" })
	public User UpdateUser(@PathVariable("id") int id,@RequestParam("first_name") String first_name, @RequestParam("last_name") String last_name,@RequestParam("email") String email) {
		User student= userservice.getUser(id);
		student.setEmail(email);
		student.setFirst_name(first_name);
		student.setLast_name(last_name);
		userservice.updateUser(student);
		return student;
	}

	@RequestMapping(value = "/addReq", method = RequestMethod.POST, produces = { "application/json",
			"application/xml" })
	public Request CreateRequest(@RequestParam("user_id") int user_id, @RequestParam("family_income") int family_income,
			@RequestParam("no_of_study_bros") int no_of_study_bros, @RequestParam("comes_from") String comes_from,
			@RequestParam("study_years") int study_years, @RequestParam("accom_years") int accom_years,
			@RequestParam("req_dep") String req_dep) {
		Request request = new Request(user_id, family_income, no_of_study_bros, comes_from, study_years, accom_years,
				req_dep);
		System.out.println(request);
		requestservice.saveRequest(request);
		StudentRights s_rights = rightsservice.getStudentsRights(user_id);
		s_rights.setCreate_req(false);
		rightsservice.updateStudentRights(s_rights);
		return request;

	}

	@RequestMapping(value = "/allAccreq", method = RequestMethod.GET, produces = { "application/json",
			"application/xml" })
	public List<AcceptedRequests> getCustomers() {
		List<AcceptedRequests> accreqs = testedrequestsservice.getAcceptedRequests();
		return accreqs;
	}

	@RequestMapping(value = "/getRights/{id}", method = RequestMethod.GET, produces = { "application/json",
			"application/xml" })
	public StudentRights getRights(@PathVariable("id") int id) {
		StudentRights st_rights = rightsservice.getStudentsRights(id);
		return st_rights;
	}
}
