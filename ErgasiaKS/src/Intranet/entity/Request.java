package Intranet.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table
public class Request {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	int req_id;
	int user_id;
	int family_income;
	int no_of_study_bros;
	String comes_from;
	int study_years;
	int accom_years;
	String req_dep;
	public Request() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Request( int user_id, int family_income, int no_of_study_bros, String comes_from,
			int study_years, int accom_years,String req_dep) {
		super();
		this.user_id = user_id;
		this.family_income = family_income;
		this.no_of_study_bros = no_of_study_bros;
		this.comes_from = comes_from;
		this.study_years = study_years;
		this.accom_years = accom_years;
		this.req_dep= req_dep;
	}
	public int getReq_id() {
		return req_id;
	}
	public void setReq_id(int req_id) {
		this.req_id = req_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getFamily_income() {
		return family_income;
	}
	public void setFamily_income(int family_income) {
		this.family_income = family_income;
	}
	public int getNo_of_study_bros() {
		return no_of_study_bros;
	}
	public void setNo_of_study_bros(int no_of_study_bros) {
		this.no_of_study_bros = no_of_study_bros;
	}
	public String getComes_from() {
		return comes_from;
	}
	public void setComes_from(String comes_from) {
		this.comes_from = comes_from;
	}
	public int getStudy_years() {
		return study_years;
	}
	public void setStudy_years(int study_years) {
		this.study_years = study_years;
	}
	public int getAccom_years() {
		return accom_years;
	}
	public void setAccom_years(int accom_years) {
		this.accom_years = accom_years;
	}
	
	public String getReq_dep() {
		return req_dep;
	}
	public void setReq_dep(String req_dep) {
		this.req_dep = req_dep;
	}
	public int GetPointsOfRequest(Request req) {
		int points = 0;
		if (req.getFamily_income()==0) {
			points=points+1000;
		}else {
			if (req.getFamily_income()<10000) {
				points=points+100;
			}else if (req.getFamily_income()<15000) {
				points=points+30;
			}else {
				points=points+0;
			}	
		}for (int i=0; i<req.getNo_of_study_bros();i++) {
			points=points+20;
		}if (!"Athens".equals(req.getComes_from())) {
			points=points+50;
		}for (int i=0;i<req.getAccom_years();i++) {
			points=points-10;
		}
		return points;
		
	}
	@Override
	public String toString() {
		return "Request [req_id=" + req_id + ", user_id=" + user_id  + ", family_income="
				+ family_income + ", no_of_study_bros=" + no_of_study_bros + ", comes_from=" + comes_from
				+ ", study_years=" + study_years + ", accom_years=" + accom_years + "]";
	}
	
}
