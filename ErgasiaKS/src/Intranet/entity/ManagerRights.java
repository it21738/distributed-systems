package Intranet.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table
public class ManagerRights {
	@Id
	int user_id;
	@Type(type="yes_no")
	boolean activate_student;
	@Type(type="yes_no")
	boolean handle_request;
	
	public ManagerRights() {
	}
	public ManagerRights(int user_id, boolean activate_student, boolean handle_request) {
		super();
		this.user_id = user_id;
		this.activate_student = activate_student;
		this.handle_request = handle_request;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public boolean isActivate_student() {
		return activate_student;
	}
	public void setActivate_student(boolean activate_student) {
		this.activate_student = activate_student;
	}
	public boolean isHandle_request() {
		return handle_request;
	}
	public void setHandle_request(boolean handle_request) {
		this.handle_request = handle_request;
	}
	@Override
	public String toString() {
		return "ManagerRights [user_id=" + user_id + ", activate_student="
				+ activate_student + ", handle_request=" + handle_request + "]";
	}
	
}
