package Intranet.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.sql.Insert;
import org.springframework.beans.factory.annotation.Autowired;

import Intranet.Service.UserService;

@Entity
@Table
public class Action {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int action_id;
	@Column(length = 45, nullable = false)
	String action_title;
	@Column(length = 45, nullable = false)
	int action_rights;

	public Action() {

	}

	public Action(String action_title, int action_rights) {
		super();
		this.action_title = action_title;
		this.action_rights = action_rights;
	}
	public ArrayList<Action> createManagersActions(){
		Action activate_a_student = new Action("Activate a student",1);
		Action handle_a_request = new Action("Handle a request",1);
		ArrayList<Action>managersActions=new ArrayList<>();
		managersActions.add(activate_a_student);
		managersActions.add(handle_a_request);
		return managersActions;
		}
	public ArrayList<Action> createChiefsActions(){
		Action activate_a_student = new Action("Activate a student",2);
		Action handle_a_request = new Action("Handle a request",2);
		Action upd_no_pos = new Action("Update the numbers of positions",2);
		ArrayList<Action>chiefsActions=new ArrayList<>();
		chiefsActions.add(upd_no_pos);
		chiefsActions.add(activate_a_student);
		chiefsActions.add(handle_a_request);
		return chiefsActions;
		}
	public ArrayList<Action> createStudentssActions(){
		Action create_a_req = new Action("Create a request",3);
		Action view_rank = new Action("View my ranking",3);
		Action view_res = new Action("View results",3);
		Action change_com_cred = new Action("Change my communication credentials",3);
		ArrayList<Action>studentsActions=new ArrayList<>();
		studentsActions.add(create_a_req);
		studentsActions.add(view_rank);
		studentsActions.add(view_res);
		studentsActions.add(change_com_cred);
		return studentsActions;
		}
	public int getAction_id() {
		return action_id;
	}

	public void setAction_id(int action_id) {
		this.action_id = action_id;
	}

	public String getAction_title() {
		return action_title;
	}

	public void setAction_title(String action_title) {
		this.action_title = action_title;
	}

	public int getAction_rights() {
		return action_rights;
	}

	public void setAction_rights(int action_rights) {
		this.action_rights = action_rights;
	}

	@Override
	public String toString() {
		return "Action [action_id=" + action_id + ", action_title=" + action_title + ", action_rights=" + action_rights
				+ "]";
	}

}
