package Intranet.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table
public class ChiefRights {
	@Id
	int user_id;
	@Type(type="yes_no")
	boolean activate_student;
	@Type(type="yes_no")
	boolean handle_request;
	@Type(type="yes_no")
	boolean update_pos;
	@Type(type="yes_no")
	boolean exp_reqs;
	public ChiefRights() {
	}
	public ChiefRights(int user_id, boolean activate_student, boolean handle_request, boolean update_pos,boolean exp_reqs) {
		super();
		this.user_id = user_id;
		this.activate_student = activate_student;
		this.handle_request = handle_request;
		this.update_pos = update_pos;
		this.exp_reqs = exp_reqs;

	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public boolean getActivate_student() {
		return activate_student;
	}
	public void setActivate_student(boolean activate_student) {
		this.activate_student = activate_student;
	}
	public boolean getHandle_request() {
		return handle_request;
	}
	public void setHandle_request(boolean handle_request) {
		this.handle_request = handle_request;
	}
	public boolean getUpdate_pos() {
		return update_pos;
	}
	public void setUpdate_pos(boolean update_pos) {
		this.update_pos = update_pos;
	}
	
	public boolean isExp_reqs() {
		return exp_reqs;
	}
	public void setExp_reqs(boolean exp_reqs) {
		this.exp_reqs = exp_reqs;
	}
	@Override
	public String toString() {
		return "ChiefRights [user_id=" + user_id + ", activate_student=" + activate_student
				+ ", handle_request=" + handle_request + ", update_pos=" + update_pos + "]";
	}
	
}
