package Intranet.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class RefusedRequests {
	@Id
	int req_id;
	int user_id;
	public RefusedRequests() {
		super();
		
	}
	public RefusedRequests(int req_id, int user_id) {
		super();
		this.req_id = req_id;
		this.user_id = user_id;
	}
	public int getReq_id() {
		return req_id;
	}
	public void setReq_id(int req_id) {
		this.req_id = req_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	@Override
	public String toString() {
		return "RefusedRequests [req_id=" + req_id + ", user_id=" + user_id + "]";
	}
	
}
