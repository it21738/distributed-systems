package Intranet.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class AcceptedRequests {
	@Id
	int req_id;
	int user_id;
	int points;
	String department;
	public AcceptedRequests() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AcceptedRequests(int req_id, int user_id, int points,String department) {
		super();
		this.department=department;
		this.req_id = req_id;
		this.user_id = user_id;
		this.points = points;
	}
	public int getReq_id() {
		return req_id;
	}
	public void setReq_id(int req_id) {
		this.req_id = req_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	@Override
	public String toString() {
		return "AcceptedRequests [req_id=" + req_id + ", user_id=" + user_id + ", points=" + points + ", department="
				+ department + "]";
	}
	
	
}
