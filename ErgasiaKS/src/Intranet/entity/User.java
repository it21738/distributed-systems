package Intranet.entity;
import static javax.persistence.GenerationType.AUTO;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Type;
@Entity
@Table
@org.hibernate.annotations.Entity(dynamicInsert = true)
public class User  {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)	
	int id;
	@Column(length = 45, nullable = false)
	String role;
	@Column(length = 45, nullable = false)
	public String first_name;
    @Column(length = 56, nullable = false)
	public String last_name;
    @Column(length = 45, nullable = false)
	public String email;
    @Column(length = 45, nullable = false)
  	public String username;
    @Column(columnDefinition = "boolean default true", nullable = false)
    private boolean enabled  = true;
	public String password;
	public String department;
    public ArrayList <String>actions;
	static int role_id;
	static String old_username;
	static String old_email;
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public User(String role, String first_name, String last_name,String username, String email, String password,
			String department, ArrayList<String> actions) {
		super();
		this.role = role;
		this.first_name = first_name;
		this.last_name = last_name;
		this.username=username;
		this.email = email;
		this.password = password;
		this.department = department;
		this.actions = actions;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	public ArrayList<String> getActions() {
		return actions;
	}

	public void setActions(ArrayList<String> actions) {
		this.actions = actions;
	}

	public int getRole_id() {
		return role_id;
	}

	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	
	public String getOld_username() {
		return old_username;
	}

	public void setOld_username(String old_username) {
		User.old_username = old_username;
	}
	
	public String getOld_email() {
		return old_email;
	}

	public void setOld_email(String old_email) {
		User.old_email = old_email;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", role=" + role + ", first_name=" + first_name + ", last_name=" + last_name
				+ ", email=" + email + ", password=" + password + ", department=" + department + ", actions=" + actions
				+ "]";
	}
	
	
	
	
	
	
	
	
	
	
	
}
