package Intranet.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table
public class StudentRights {
	@Id
	int user_id;
	@Type(type="yes_no")
	boolean create_req;
	@Type(type="yes_no")
	boolean view_rank;
	@Type(type="yes_no")
	boolean view_res;
	@Type(type="yes_no")
	boolean change_com;
	
	public StudentRights() {
	}

	public StudentRights(int user_id, boolean create_req, boolean view_rank, boolean view_res, boolean change_com) {
		super();
		this.user_id = user_id;
		this.create_req = create_req;
		this.view_rank = view_rank;
		this.view_res = view_res;
		this.change_com = change_com;
	}
	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public boolean isCreate_req() {
		return create_req;
	}

	public void setCreate_req(boolean create_req) {
		this.create_req = create_req;
	}

	public boolean isView_rank() {
		return view_rank;
	}

	public void setView_rank(boolean view_rank) {
		this.view_rank = view_rank;
	}

	public boolean isView_res() {
		return view_res;
	}

	public void setView_res(boolean view_res) {
		this.view_res = view_res;
	}

	public boolean isChange_com() {
		return change_com;
	}

	public void setChange_com(boolean change_com) {
		this.change_com = change_com;
	}

	@Override
	public String toString() {
		return "StudentRights [user_id=" + user_id + ", create_req=" + create_req
				+ ", view_rank=" + view_rank + ", view_res=" + view_res + ", change_com=" + change_com + "]";
	}
	
}
