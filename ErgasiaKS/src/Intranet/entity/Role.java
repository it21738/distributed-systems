package Intranet.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Role {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)	
	int role_id;
	@Column(length = 45, nullable = false)
	String role_title;
	@Column(length = 45,nullable = false)
	int role_rights;
	public Role(String role_title,int role_rights) {
		super();
		this.role_title = role_title;
		this.role_rights = role_rights;
	}
	
	public Role() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getRole_title() {
		return role_title;
	}
	public void setRole_title(String role_title) {
		this.role_title = role_title;
	}
	public int getRole_id() {
		return role_id;
	}
	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}
	public int getRole_rights() {
		return role_rights;
	}
	public void setRole_rights(int role_rights) {
		this.role_rights = role_rights;
	}
	@Override
	public String toString() {
		return "Role [role_title=" + role_title + ", role_id=" + role_id + ", role_rights=" + role_rights + "]";
	}
	
}
