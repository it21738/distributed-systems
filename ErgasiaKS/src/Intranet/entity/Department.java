package Intranet.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Department {
	@Id
	int dept_id;
	//int manager_id;
	String dept_name;
	int free_pos;
	int dept_pos;
	
	
	public Department() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Department(int dept_id, String dept_name, int free_pos, int dept_pos) {
		super();
		this.dept_id = dept_id;
		this.dept_name = dept_name;
		this.free_pos = free_pos;
		this.dept_pos = dept_pos;
	}


	


	public int getDept_id() {
		return dept_id;
	}


	public void setDept_id(int dept_id) {
		this.dept_id = dept_id;
	}


	public String getDept_name() {
		return dept_name;
	}


	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}


	public int getFree_pos() {
		return free_pos;
	}


	public void setFree_pos(int free_pos) {
		this.free_pos = free_pos;
	}


	public int getDept_pos() {
		return dept_pos;
	}


	public void setDept_pos(int dept_pos) {
		this.dept_pos = dept_pos;
	}


	@Override
	public String toString() {
		return "Department [dept_id=" + dept_id + ", dept_name=" + dept_name
				+ ", free_pos=" + free_pos + ", dept_pos=" + dept_pos + "]";
	}
}
