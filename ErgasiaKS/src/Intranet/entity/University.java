package Intranet.entity;



import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table
public class University {
	@Id
	int un_id;
	String un_name;
	String un_address;
	int year;
	String city;
	int total_pos;
	
	public University() {
		super();
		// TODO Auto-generated constructor stub
	}
	public University(int un_id,String un_name, String un_address, int year,String city,int total_pos) {
		super();
		this.un_id = un_id;		
		this.un_name = un_name;
		this.un_address = un_address;
		this.year = year;
		this.city=city;
		this.total_pos = total_pos;
	}
	public String getUn_name() {
		return un_name;
	}
	public void setUn_name(String un_name) {
		this.un_name = un_name;
	}
	public String getUn_address() {
		return un_address;
	}
	public void setUn_address(String un_address) {
		this.un_address = un_address;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getTotal_pos() {
		return total_pos;
	}
	public void setTotal_pos(int total_pos) {
		this.total_pos = total_pos;
	}
	
	public int getUn_id() {
		return un_id;
	}
	public void setUn_id(int un_id) {
		this.un_id = un_id;
	}

	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "University [un_name=" + un_name + ", un_address=" + un_address +  ", year=" + year + ", total_pos="
				+ total_pos + "]";
	}
	
}

