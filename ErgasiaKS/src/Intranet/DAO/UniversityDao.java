package Intranet.DAO;

import java.util.List;

import Intranet.entity.University;

public interface UniversityDao {
	 public List<University> getUniversities();
	 public void saveUniversity(University university) ;
	 public University getUniversity(int un_id);
	 public void updateUniversity(University university);
	 public void deleteUniversity(int un_id);
}
