package Intranet.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import Intranet.entity.Department;
import Intranet.entity.University;
@Repository
public class UniversityDaoImpl implements UniversityDao {
	@Autowired
	SessionFactory sessionFactory;
	@Override
	public List<University> getUniversities() {
		Session currentSession = sessionFactory.getCurrentSession();
        Query<University> query = currentSession.createQuery("from University ", University.class);
        List<University> universities = query.getResultList();
        return universities;
	}

	@Override
	public void saveUniversity(University university) {
		Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(university);
	}

	@Override
	public University getUniversity(int un_id) {
		Session currentSession = sessionFactory.getCurrentSession();
		University university = currentSession.get(University.class, un_id);
		return university;
	}

	@Override
	public void updateUniversity(University university) {
		Session currentSession = sessionFactory.getCurrentSession();
	    currentSession.update(university);
	}

	@Override
	public void deleteUniversity(int un_id) {
		Session currentSession = sessionFactory.getCurrentSession();
		University university = currentSession.get(University.class, un_id);
		currentSession.delete(university);
	}

}
