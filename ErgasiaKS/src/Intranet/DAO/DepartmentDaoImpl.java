package Intranet.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import Intranet.entity.Department;
@Repository
public class DepartmentDaoImpl implements DepartmentDao {
	@Autowired
    private SessionFactory sessionFactory;
	@Override
	public List<Department> getDepartments() {
		Session currentSession = sessionFactory.getCurrentSession();
        Query<Department> query = currentSession.createQuery("from Department", Department.class);
        List<Department> departments = query.getResultList();
        return departments;
	}

	@Override
	public void saveDepartment(Department department) {
		Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(department);

	}

	@Override
	public Department getDepartment(int dept_id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Department department = currentSession.get(Department.class, dept_id);
		return department;
	}

	@Override
	public void updateDepartment(Department department) {
		Session currentSession = sessionFactory.getCurrentSession();
	      currentSession.update(department);
	}

	@Override
	public void deleteDepartment(int dept_id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Department department = currentSession.get(Department.class, dept_id);
		currentSession.delete(department);
	}

}
