package Intranet.DAO;

import java.util.List;

import Intranet.entity.Request;

public interface RequestDao {
	public List<Request> getRequests();
	 public void saveRequest(Request request) ;
	 public Request getRequest(int req_id);
	 public void updateRequest( Request request);
	 public void deleteRequest(int req_id);
	 public List<Request> getRequestsByDep(String department);
	 public Request getRequestByUserId(int user_id);

}
