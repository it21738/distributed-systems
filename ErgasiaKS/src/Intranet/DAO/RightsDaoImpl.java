package Intranet.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import Intranet.entity.ChiefRights;
import Intranet.entity.ManagerRights;
import Intranet.entity.Role;
import Intranet.entity.StudentRights;
@Repository
public class RightsDaoImpl implements RightsDao {
	@Autowired
	SessionFactory sessionFactory;
	@Override
	public List<ManagerRights> getManagerRights() {
		Session currentSession = sessionFactory.getCurrentSession();
        Query<ManagerRights> query = currentSession.createQuery("from ManagerRights ", ManagerRights.class);
        List<ManagerRights> manager_rights = query.getResultList();
        return manager_rights;
	}

	@Override
	public void saveManagerRights(ManagerRights manager_rights) {
		Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(manager_rights);
	}

	@Override
	public ManagerRights getManagersRights(int user_id) {
		Session currentSession = sessionFactory.getCurrentSession();		
		ManagerRights manager_rights = currentSession.get(ManagerRights.class, user_id);
		if(manager_rights==null) {
			return null;
		}else {
			return manager_rights;
		}
	}

	@Override
	public void updateManagerRights(ManagerRights manager_rights) {
		Session currentSession = sessionFactory.getCurrentSession();
	      currentSession.saveOrUpdate(manager_rights);
	}

	@Override
	public void deleteManagerRights(int user_id) {
		Session currentSession = sessionFactory.getCurrentSession();
		ManagerRights manager_rights = currentSession.get(ManagerRights.class, user_id);
		currentSession.delete(manager_rights);		
	}

	@Override
	public List<ChiefRights> getChiefRights() {
		Session currentSession = sessionFactory.getCurrentSession();
        Query<ChiefRights> query = currentSession.createQuery("from ChiefRights ", ChiefRights.class);
        List<ChiefRights> chief_rights = query.getResultList();
        return chief_rights;
	}

	@Override
	public void saveChiefRights(ChiefRights chief_rights) {
		Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(chief_rights);
	}

	@Override
	public ChiefRights getChiefsRights(int user_id) {
		Session currentSession = sessionFactory.getCurrentSession();		
		ChiefRights chief_rights = currentSession.get(ChiefRights.class, user_id);
		if(chief_rights==null) {
			return null;
		}else {
			return chief_rights;
		}
		
	}

	@Override
	public void updateChiefRights(ChiefRights chief_rights) {
		Session currentSession = sessionFactory.getCurrentSession();
	      currentSession.saveOrUpdate(chief_rights);
	}

	@Override
	public void deleteChiefRights(int user_id) {
		Session currentSession = sessionFactory.getCurrentSession();
		ChiefRights chief_rights = currentSession.get(ChiefRights.class, user_id);
		currentSession.delete(chief_rights);		
	}

	@Override
	public List<StudentRights> getStudentRights() {
		Session currentSession = sessionFactory.getCurrentSession();
        Query<StudentRights> query = currentSession.createQuery("from StudentRights ", StudentRights.class);
        List<StudentRights> student_rights = query.getResultList();
        return student_rights;
	}

	@Override
	public void saveStudentRights(StudentRights student_rights) {
		// TODO Auto-generated method stub
		Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(student_rights);
	}

	@Override
	public StudentRights getStudentsRights(int user_id) {
		Session currentSession = sessionFactory.getCurrentSession();		
		StudentRights student_rights = currentSession.get(StudentRights.class, user_id);
		if(student_rights==null) {
			return null;
		}else {
			return student_rights;
		}
	}

	@Override
	public void updateStudentRights(StudentRights student_rights) {
		Session currentSession = sessionFactory.getCurrentSession();
	      currentSession.saveOrUpdate(student_rights);		
	}

	@Override
	public void deleteStudentRights(int user_id) {
		Session currentSession = sessionFactory.getCurrentSession();
		StudentRights student_rights = currentSession.get(StudentRights.class, user_id);
		currentSession.delete(student_rights);
	}

}
