package Intranet.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import Intranet.entity.Role;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
@Repository
public class RoleDaoImpl implements RoleDao {
	@Autowired
	SessionFactory sessionFactory;
	@Override
	public List<Role> getRoles() {
		
		Session currentSession = sessionFactory.getCurrentSession();
        Query<Role> query = currentSession.createQuery("from Role ", Role.class);
        List<Role> roles = query.getResultList();
        return roles;
}
	

	@Override
	public void saveRole(Role role) {
		Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(role);
	}

	@Override
	public Role getRole(int id) {
		Session currentSession = sessionFactory.getCurrentSession();		
		Role role = currentSession.get(Role.class, id);
		return role;
	}

	@Override
	public void updateRole(Role role) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("update Role set role_id=:role_id,role_title=:role_title,role_rights=:role_rights where role_id =:role_id");
	    query.setInteger("role_id", role.getRole_id());
	    query.setString("role_title", role.getRole_title());
	    query.setInteger("role_rights", role.getRole_rights());
	    int result = query.executeUpdate();
				

	}

	@Override
	public void deleteRole(int id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Role role = currentSession.get(Role.class, id);
		currentSession.delete(role);		
	}

}
