package Intranet.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import Intranet.entity.AcceptedRequests;
import Intranet.entity.RefusedRequests;
import Intranet.entity.User;
@Repository
public class TestedRequestsDaoImpl implements TestedRequestsDao {
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<AcceptedRequests> getAcceptedRequests() {
		Session currentSession = sessionFactory.getCurrentSession();
        Query<AcceptedRequests> query = currentSession.createQuery("from AcceptedRequests order by points desc", AcceptedRequests.class);
        List<AcceptedRequests> accepted_requests = query.getResultList();
        return accepted_requests;
	}

	@Override
	public void saveAcceptedRequest(AcceptedRequests acceptedreq) {
		Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(acceptedreq);
	}

	@Override
	public void deleteAcceptedRequests(int req_id) {
		Session currentSession = sessionFactory.getCurrentSession();
		AcceptedRequests accepted_requests = currentSession.get(AcceptedRequests.class, req_id);
		currentSession.delete(accepted_requests);
	}

	@Override
	public void updateAcceptedRequest(AcceptedRequests acceptedreq) {
		Session currentSession = sessionFactory.getCurrentSession();
	      currentSession.update(acceptedreq);
	}

	@Override
	public AcceptedRequests getAcceptedRequest(int req_id) {
		Session currentSession = sessionFactory.getCurrentSession();		
		AcceptedRequests accepted_requests = currentSession.get(AcceptedRequests.class, req_id);
		if(accepted_requests==null) {
			return null;
		}else {
			return accepted_requests;
		}
	}
	@Override
	public List<AcceptedRequests> getAcceptedRequestsByDepartment(String department) {
		Session currentSession = sessionFactory.getCurrentSession();
        Query<AcceptedRequests> query = currentSession.createQuery("from AcceptedRequests where department=:department order by points DESC ", AcceptedRequests.class);
        query.setParameter("department", department);
        List<AcceptedRequests> accepted_requests = query.getResultList();
        return accepted_requests;
	}

	@Override
	public AcceptedRequests getAcceptedRequestByUserId(int user_id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query<AcceptedRequests> query = currentSession.createQuery("from AcceptedRequests where user_id=:user_id", AcceptedRequests.class);
	    query.setParameter("user_id", user_id);
	    if(query.list().isEmpty()) {
	    	return null;
	    }else {
	    	return (AcceptedRequests) query.list().get(0);
	    }
	}

	
	

}
