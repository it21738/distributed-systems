package Intranet.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import Intranet.entity.Action;
import Intranet.entity.Role;
@Repository
public class ActionDaoImpl implements ActionDao {
	@Autowired
    private SessionFactory sessionFactory;
	@Override
	public List<Action> getActions() {
        Session currentSession = sessionFactory.getCurrentSession();
        Query<Action> query = currentSession.createQuery("from Action", Action.class);
        List<Action> actions = query.getResultList();
        return actions;
	}
	@Override
	public void saveAction(Action action) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(action);
	}
	@Override
	public void deleteAction(int id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Action action = currentSession.get(Action.class, id);
		currentSession.delete(action);
	}
	@Override
	public void updateAction(Action action) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("update Action set action_id=:action_id,action_title=:action_title,action_rights=:action_rights where action_id =:action_id");
	    query.setInteger("action_id", action.getAction_id());
	    query.setString("action_title", action.getAction_title());
	    query.setInteger("action_rights", action.getAction_rights());
	    int result = query.executeUpdate();
	}
	@Override
	public Action getAction(int id) {
		// 
		Session currentSession = sessionFactory.getCurrentSession();		
		Action action = currentSession.get(Action.class, id);
		return action;
	}
}
