package Intranet.DAO;

import java.util.List;
import Intranet.entity.User;

public interface UserDao {
	
	 public List<User> getUsers();
	 public void saveUser(User user) ;
	 public User getUser(int id);
	 public void updateUser(User user);
	 public void deleteUser(int id);
	 public User getUserbyUsername(String username);
	 public List<User> getUsersbyRole(String role,String department);
	 public User getStudent(String username);

	
}
