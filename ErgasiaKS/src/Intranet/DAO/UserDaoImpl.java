package Intranet.DAO;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import Intranet.entity.User;

@Repository
public class UserDaoImpl implements UserDao {
	@Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public List<User> getUsers() {
            Session currentSession = sessionFactory.getCurrentSession();
            Query<User> query = currentSession.createQuery("from User order by last_name", User.class);
            List<User> users = query.getResultList();
            return users;
    }
    @Override
	public void saveUser(User user) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(user);
	
    }
    @Override
	public User getUser(int id) {
		Session currentSession = sessionFactory.getCurrentSession();
		User user = currentSession.get(User.class, id);
		return user;
	}
    @Override
	public User getUserbyUsername(String username) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from User where username=:username");
	    query.setParameter("username", username);
	    if(query.list().isEmpty()) {
	    	return null;
	    }else {
	    	return (User) query.list().get(0);
	    }
	}

	@Override
	public void deleteUser(int id) {
		Session currentSession = sessionFactory.getCurrentSession();
		User user = currentSession.get(User.class, id);
		currentSession.delete(user);
		
	}
	@Override
     public void updateUser(User user ) {
      Session currentSession = sessionFactory.getCurrentSession();
      currentSession.update(user);
      
      
     }
	@Override
	public List<User> getUsersbyRole(String role,String department) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query<User> query = currentSession.createQuery("from User where role=:role and department=:department",User.class);
	    query.setParameter("role", role);
	    query.setParameter("department", department);
	    List<User> users = query.getResultList();
	    return users;
	}
	@Override
	public User getStudent(String username) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("from User where username=:username and role='ROLE_STUDENT'");
	    query.setParameter("username", username);
	    if(query.list().isEmpty()) {
	    	return null;
	    }else {
	    	return (User) query.list().get(0);
	    }
	}
}    
    
