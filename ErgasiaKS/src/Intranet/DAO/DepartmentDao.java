package Intranet.DAO;

import java.util.List;

import Intranet.entity.Department;

public interface DepartmentDao {
	public List<Department> getDepartments();
	 public void saveDepartment(Department department) ;
	 public Department getDepartment(int dept_id);
	 public void updateDepartment(Department department);
	 public void deleteDepartment(int dept_id);
}
