package Intranet.DAO;

import java.util.List;

import Intranet.entity.Action;
import Intranet.entity.Role;

public interface ActionDao {
	public List<Action> getActions();
	public void saveAction(Action action) ;
	public void deleteAction(int id);
	public void updateAction(Action action);
	public Action getAction(int id);

}
