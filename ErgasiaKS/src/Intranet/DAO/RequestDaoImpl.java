package Intranet.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import Intranet.entity.Request;
import Intranet.entity.User;
@Repository
public class RequestDaoImpl implements RequestDao {
	@Autowired
    SessionFactory sessionFactory;
	@Override
	public List<Request> getRequests() {
		Session currentSession = sessionFactory.getCurrentSession();
        Query<Request> query = currentSession.createQuery("from Request ", Request.class);
        List<Request> requests = query.getResultList();
        return requests;
	}

	@Override
	public void saveRequest(Request request) {
		Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(request);
	}

	@Override
	public Request getRequest(int req_id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Request request = currentSession.get(Request.class, req_id);
		return request;
	}

	@Override
	public void updateRequest(Request request) {
		Session currentSession = sessionFactory.getCurrentSession();
	      currentSession.update(request);
	}

	@Override
	public void deleteRequest(int req_id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Request request = currentSession.get(Request.class, req_id);
		currentSession.delete(request);
	}

	@Override
	public List<Request> getRequestsByDep(String department) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query<Request> query = currentSession.createQuery("from Request where req_dep=:req_dep", Request.class);
		query.setParameter("req_dep", department);
        List<Request> requests = query.getResultList();
		return requests;
	}

	@Override
	public Request getRequestByUserId(int user_id) {
		Session currentSession = sessionFactory.getCurrentSession();		
		Query<Request> query = currentSession.createQuery("from Request where user_id=:user_id", Request.class);
		query.setParameter("user_id", user_id);
	    if(query.list().isEmpty()) {
	    	return null;
	    }else {
	    	return (Request) query.list().get(0);
	    }
	}

}
