from django import forms

class StudentForm(forms.Form):
    username = forms.CharField( widget=forms.TextInput(attrs={'class' : 'form-control custom-input'}))
    password = forms.CharField( widget=forms.PasswordInput(attrs={'class' : 'form-control custom-input'}))
    widgets = {
            'password': forms.PasswordInput(),
        }
class RequestForm(forms.Form):
    CHOICES = (('0', '0'),('1', '1'),('2', '2'),('3', '3'),('4', '4'),('5', '5+'))
    check_parents = forms.BooleanField(label='Check here if you and your parents are unemployed',required=False)
    family_income = forms.IntegerField(label='Family Income:',required=False,initial=0)
    comes_from= forms.CharField(label='Where you come from:', max_length=100)
    no_of_study_bros = forms.ChoiceField(label='Number of study brothers',choices=CHOICES)
    study_years=forms.ChoiceField(label='Study years',choices=CHOICES)
    accom_years=forms.ChoiceField(label='Years of free Accomodation',choices=CHOICES)
