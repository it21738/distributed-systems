from django.apps import AppConfig


class StudentsfrontConfig(AppConfig):
    name = 'studentsFront'
