from django.urls import path
from . import views
urlpatterns = [
    path('',views.home,name='home'),
    path('student/entry',views.studentEntry,name='StudentEntry'),
    path('student/makeReq',views.makeRequest,name='MakeRequest'),
    path('student/viewRank',views.viewRanking,name='StudentEntry'),
    path('student/viewRes',views.viewResults,name='StudentEntry'),
]
