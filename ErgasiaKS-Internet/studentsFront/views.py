from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
import requests
import json
from .forms import StudentForm
from .forms import RequestForm
from django.contrib import messages
Student={}
def home(request):
    if request.method == 'POST':
        form = StudentForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            url='http://localhost:8080/ErgasiaKS/api/student/getStudent'
            login_student={'username' : username,'password' : password}
            r=requests.post(url,data=login_student)
            print(r.text)
            if r.text:
                y=json.loads(r.text)
                Student.update(y)
                print(Student)
                messages.success(request, 'Welcome '+Student["username"] +'!')
                return HttpResponseRedirect('student/entry')
            else:
                messages.warning(request, 'Login failed check your Username or password')
                form = StudentForm()
    else:
        form = StudentForm()
    return render(request, 'studentsFront/Home.html',context={'title':'Home page','form': form})
def studentEntry(request):
    if request.method == 'POST':
        id = Student["id"]
        first_name = request.POST['first_name']
        last_name=request.POST['last_name']
        email=request.POST['email']
        url='http://localhost:8080/ErgasiaKS/api/student/update/'+str(id)
        student_details={'first_name' : first_name,'last_name': last_name,'email':email}
        r=requests.post(url,data=student_details)
        if r.text:
            y=json.loads(r.text)
            Student.update(y)
            print(Student)
            messages.success(request, 'Details updated successfully!')
            return HttpResponseRedirect('entry')
    else:
        return render(request, 'studentsFront/StudentEntry.html',context={'title':'Welcome Student','Student' : Student})
def makeRequest(request):
    id = Student["id"]
    url='http://localhost:8080/ErgasiaKS/api/student/getRights/'+str(id)
    r=requests.get(url)
    student_rights=json.loads(r.text)
    print(student_rights)
    create_req=student_rights["create_req"]
    print (create_req)
    if create_req==True:
        if request.method == 'POST':
            form = RequestForm(request.POST)
            if form.is_valid():
                if request.POST.get("check_parents"):
                    family_income=0
                else:
                    family_income = form.cleaned_data['family_income']
                comes_from = form.cleaned_data['comes_from']
                no_of_study_bros = form.cleaned_data['no_of_study_bros']
                study_years = form.cleaned_data['study_years']
                accom_years = form.cleaned_data['accom_years']
                user_id=Student["id"]
                req_dep=Student["department"]
                req={
                'user_id' : user_id,
                'family_income': family_income,
                'no_of_study_bros': no_of_study_bros,
                'comes_from': comes_from,
                'study_years': study_years,
                'accom_years' : accom_years,
                'req_dep' : req_dep
                }
                r = requests.post('http://localhost:8080/ErgasiaKS/api/student/addReq', data=req)
                messages.success(request, 'Τhe request was successfully sent')
                return HttpResponseRedirect('entry')
            else:
                form = RequestForm()
                return render(request, 'studentsFront/MakeRequest.html',context={'title':'Make Request','form': form,'Student' : Student})
        else:
            form = RequestForm()
            return render(request, 'studentsFront/MakeRequest.html',context={'title':'Make Request','form': form,'Student' : Student})
    else:
        messages.warning(request, 'Too early!You have to wait for the worker to activate you !')
        return HttpResponseRedirect('entry')
def viewRanking(request):
    id = Student["id"]
    print(id)
    url='http://localhost:8080/ErgasiaKS/api/student/getRights/'+str(id)
    r=requests.get(url)
    student_rights=json.loads(r.text)
    print(student_rights)
    view_rank=student_rights["view_rank"]
    print (view_rank)
    if view_rank==True:
        url='http://localhost:8080/ErgasiaKS/api/student/allAccreq'
        r=requests.get(url)
        allAccreq=json.loads(r.text)
        print(allAccreq)
        i=0
        for item in allAccreq:
            i+=1
            if item['user_id'] == id:
                my_accreq = item
                print(my_accreq)
                rank=i
                messages.success(request, 'Your rank is: '+str(rank)+'.Just wait for the results to be sure! ')
                break
        else:
            my_item = None
            messages.success(request, 'Sorry your request has been refused!')
    else:
        messages.warning(request, 'Too early!You have to wait for the worker to review your request!')
    return HttpResponseRedirect('entry')
def viewResults(request):
    id = Student["id"]
    url='http://localhost:8080/ErgasiaKS/api/student/getRights/'+str(id)
    r=requests.get(url)
    if r.text:
        student_rights=json.loads(r.text)
        print(student_rights)
        view_res=student_rights["view_res"]
        print (view_res)
        if view_res==True:
            url='http://localhost:8080/ErgasiaKS/api/student/allAccreq'
            r=requests.get(url)
            allAccreq=json.loads(r.text)
            for item in allAccreq:
                if item['user_id'] == id:
                    my_accreq = item
                    print(my_accreq)
                    messages.success(request, 'Congratulations!Your request for accomodation has been accepted!')
                    break
            else:
                my_item = None
                messages.success(request, 'Your request for accomodation has been refused!')
        else:
            messages.warning(request, 'Too early!You have to wait for the worker to update the results!')
        return HttpResponseRedirect('entry')
